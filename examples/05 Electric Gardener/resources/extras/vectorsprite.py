import bee
prim = bee._import("resources/extras/primitives.py")

class VectorSprite:
	def __init__(self, path):
		self.path = path
		self.primitives = []

		if not bee.fs.exists(self.path):
			return

		data = bee.fs.get_file(self.path)["get"]()
		for line in data.splitlines():
			params = [p for p in line.split("\t") if len(p) > 0]
			if len(params) == 0 or params[0][0] == '#':
				continue
			elif params[0] == "line":
				self.primitives.append(prim.PrimitiveLine(
					(float(params[1]), float(params[2]), float(params[3])),
					(float(params[4]), float(params[5]), float(params[6]))
				))
			elif params[0] == "triangle":
				self.primitives.append(prim.PrimitiveTriangle(
					(float(params[1]), float(params[2]), float(params[3])),
					(float(params[4]), float(params[5]), float(params[6])),
					(float(params[7]), float(params[8]), float(params[9]))
				))
			elif params[0] == "quad":
				self.primitives.append(prim.PrimitiveQuad(
					(float(params[1]), float(params[2]), float(params[3])),
					(float(params[4]), float(params[5]), float(params[6]))
				))
			elif params[0] == "polygon":
				self.primitives.append(prim.PrimitivePolygon(
					(float(params[1]), float(params[2]), float(params[3])),
					float(params[4]),
					float(params[5]),
					float(params[6]),
					float(params[7])
				))
			elif params[0] == "arc":
				self.primitives.append(prim.PrimitiveArc(
					(float(params[1]), float(params[2]), float(params[3])),
					float(params[4]),
					float(params[5]),
					float(params[6])
				))
			elif params[0] == "circle":
				self.primitives.append(prim.PrimitiveCircle(
					(float(params[1]), float(params[2]), float(params[3])),
					float(params[4])
				))

	def draw(self, pos, rot, color):
		for p in self.primitives:
			p.draw(pos, rot, color)

vs_player = VectorSprite("resources/extras/player.csv")
vs_enemy = VectorSprite("resources/extras/enemy.csv")
vs_bee = VectorSprite("resources/extras/bee.csv")
