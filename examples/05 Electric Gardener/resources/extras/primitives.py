import bee

def _add(p1, p2):
	return (
		p1[0] + p2[0],
		p1[1] + p2[1],
		p1[2] + p2[2]
	)

class EPrimitive:
	LINE = 0
	TRIANGLE = 1
	QUAD = 2
	POLYGON = 3
	ARC = 4
	CIRCLE = 5

class Primitive:
	def draw(self, offset, rot, color):
		return 0

class PrimitiveLine(Primitive):
	def __init__(self, p1, p2):
		self.p1 = p1
		self.p2 = p2

	def draw(self, offset, rot, color):
		bee.render.draw_line(
			_add(self.p1, offset),
			_add(self.p2, offset),
			color
		)

class PrimitiveTriangle(Primitive):
	def __init__(self, p1, p2, p3):
		self.p1 = p1
		self.p2 = p2
		self.p3 = p3

	def draw(self, offset, rot, color):
		bee.render.draw_triangle(
			_add(self.p1, offset),
			_add(self.p2, offset),
			_add(self.p3, offset),
			color,
			False
		)

class PrimitiveQuad(Primitive):
	def __init__(self, pos, size):
		self.pos = pos
		self.size = size

	def draw(self, offset, rot, color):
		bee.render.draw_quad(
			_add(self.pos, offset),
			self.size,
			1,
			color
		)

class PrimitivePolygon(Primitive):
	def __init__(self, pos, r, a_start, a_span, amount):
		self.pos = pos
		self.radius = r
		self.angle_start = a_start
		self.angle_span = a_span
		self.segment_amount = amount

	def draw(self, offset, rot, color):
		bee.render.draw_polygon(
			_add(self.pos, offset),
			self.radius,
			self.angle_start, self.angle_span,
			self.segment_amount,
			1,
			color
		)

class PrimitiveArc(PrimitivePolygon):
	def __init__(self, pos, r, a_start, a_span):
		PrimitivePolygon.__init__(self, pos, r, a_start, a_span, a_span//4)

class PrimitiveCircle(PrimitiveArc):
	def __init__(self, pos, r):
		PrimitiveArc.__init__(self, pos, r, 0, 360)
