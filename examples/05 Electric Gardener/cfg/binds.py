# Bind keys
bind("SDLK_BACKQUOTE", "$ConsoleToggle")
bind("SDLK_ESCAPE", "$Quit")

bind("SDLK_r", "$Reset")
bind("SDLK_RIGHT", "$MoveClockwise")
bind("SDLK_LEFT", "$MoveCounterClockwise")
bind("SDLK_c", "$FirePellet")
bind("SDLK_x", "$FireBeam")
bind("SDLK_LSHIFT", "$Super")

bind("SDLK_l", "CreateLevel()")
