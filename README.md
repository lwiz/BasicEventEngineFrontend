Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>

BEEFrontend is the graphical editor for the
[BasicEventEngine](https://gitlab.com/piluke/BasicEventEngine), an
event-driven game engine under heavy development. The issue tracker for this
repository should only be used for bugs with the actual frontend, please direct
all engine bugs to the
[engine issue tracker](https://gitlab.com/piluke/BasicEventEngine/issues).

The software is still in alpha so do not expect any sort of stability. Feel
free to email me if you have any questions! Also I'm glad to accept
contributions to the engine code or feature requests, preferably via Gitlab.
And above all, report bugs! :)

p.s. See LICENSE for info about the MIT License

## How to use

1. Install wxPython:

        pip3 install wxpython

2. Open BEEF and add resources as needed (or try out an example):

        ./beef.py

3. Generate the project by selecting Build->Run.
