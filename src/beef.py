# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import wx.adv

import sys
import os
import shutil
import tempfile
import glob
import json
import itertools
import subprocess

BEEF_VERSION_MAJOR = 0
BEEF_VERSION_MINOR = 2
BEEF_VERSION_PATCH = 5

from ui.menubar import BEEFMenuBar
from ui.toolbar import BEEFToolBar
from ui.treectrl import BEEFTreeCtrl
from ui.notebook import BEEFNotebook
from ui.editdialog import BEEFEditDialog
from ui.console import BEEFConsole
from ui.options import BEEFOptions
from ui.preferences import BEEFPreferences
from ui.engine_manager import BEEFEngineManager

from core.compiler import Compiler

from resource.base import BEEFBaseResource
from resource.enum import EResource
from resource.texture import BEEFTexture
from resource.sound import BEEFSound
from resource.font import BEEFFont
from resource.path import BEEFPath
from resource.timeline import BEEFTimeline
from resource.mesh import BEEFMesh
from resource.light import BEEFLight
from resource.script import BEEFScript
from resource.object import BEEFObject
from resource.room import BEEFRoom

from resource.config import BEEFConfig
from resource.extra import BEEFExtra

class BEEFFrame(wx.Frame):
	def __init__(self, parent, id, file=None):
		wx.Frame.__init__(self, parent, id, "BEE Frontend v" + self.getVersionString(), style=wx.DEFAULT_FRAME_STYLE | wx.MAXIMIZE)
		self.parent = parent
		self.ready = False

		self.defaultPrefCfg = {
			"open_console": True,
			"default_engine": ""
		}
		self.defaultGameCfg = {
			"beef_version_major": BEEF_VERSION_MAJOR,
			"beef_version_minor": BEEF_VERSION_MINOR,
			"beef_version_patch": BEEF_VERSION_PATCH,

			"game_name": "BEE_Example",

			"game_version_major": 0,
			"game_version_minor": 1,
			"game_version_patch": 1,

			"open_resources": [],

			"resource_edit_programs": {
				EResource.TEXTURE: "",
				EResource.SOUND: "",
				EResource.FONT: "",
				EResource.PATH: "",
				EResource.TIMELINE: "",
				EResource.MESH: "",
				EResource.LIGHT: "",
				EResource.SCRIPT: "",
				EResource.OBJECT: "",
				EResource.ROOM: ""
			},

			"first_room": ""
		}

		self.Bind(wx.EVT_CLOSE, self.quit)

		self.gameCfg = {}

		self.textures = []
		self.sounds = []
		self.fonts = []
		self.paths = []
		self.timelines = []
		self.meshes = []
		self.lights = []
		self.scripts = []
		self.objects = []
		self.rooms = []
		self.configs = []
		self.extras = []

		os.chdir(self.getDir())

		self.images = {}
		for n, p in {
			"noimage": "images/noimage.png",
			"nosprite": "images/nosprite.png"
		}.items():
			self.images[n] = wx.Image(p)

		self.compiler = Compiler(self)
		self.options = BEEFOptions(self)
		self.preferences = BEEFPreferences(self)
		self.engine_manager = BEEFEngineManager(self)

		if not os.path.exists("engines/linux"):
			os.makedirs("engines/linux")
		if not os.path.exists("engines/win"):
			os.makedirs("engines/win")
		if not os.path.exists("engines/macos"):
			os.makedirs("engines/macos")

		self.init(file)

	def init(self, file=None):
		self.SetIcon(wx.Icon("images/beef.ico", wx.BITMAP_TYPE_ICO))
		self.CreateStatusBar()
		self.SetStatusText("Loading BEEF...")

		# Construct the window
		self.menubar = BEEFMenuBar(self)
		self.SetMenuBar(self.menubar)
		self.menubar.Bind()

		self.toolbar = BEEFToolBar(self)
		self.SetToolBar(self.toolbar)
		self.toolbar.Bind()
		self.toolbar.Realize()

		self.splitter = wx.SplitterWindow(self, -1, style=wx.SP_LIVE_UPDATE)
		border = wx.BORDER_SUNKEN
		self.pane1 = wx.Window(self.splitter, style=border)
		self.pane2 = wx.Window(self.splitter, style=border)
		self.splitter.SetMinimumPaneSize(50)
		self.splitter.SplitVertically(self.pane1, self.pane2, 250)

		self.treectrl = BEEFTreeCtrl(self, self.pane1)
		self.treectrl.reset()
		self.treectrl.Bind()

		self.notebook = BEEFNotebook(self, self.pane2)
		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.notebook, 1, wx.EXPAND)
		self.pane2.SetSizer(self.sizer)

		self.editDialogs = []
		for t in EResource.getAll():
			ed = BEEFEditDialog(self, EResource.get(t), None)
			self.editDialogs.append(ed)

		self.console = BEEFConsole(self, self.pane2, self.compiler.srcDir)

		# Load the preferences
		self.prefCfg = self.defaultPrefCfg
		prefFile = self.getDir()+"/preferences.json"
		if os.path.isfile(prefFile):
			with open(prefFile, "r") as f:
				prefCfg = json.loads(f.read())

			was_updated = False
			for k, v in self.prefCfg.items():
				if k not in prefCfg:
					was_updated = True
					prefCfg[k] = v
			self.prefCfg = prefCfg

			self.savePreferences()
		else:
			self.savePreferences()

		# Encourage the user to download and select an engine to use
		if not self.engine_manager.getCurrentEngines():
			d = wx.MessageDialog(self, "Please download an engine to use for your project", style=wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP | wx.CENTER)
			d.ShowModal()
			d.Destroy()

			self.engine_manager.show()
		if not self.prefCfg["default_engine"]:
			d = wx.MessageDialog(self, "Please set an engine as the default", style=wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP | wx.CENTER)
			d.ShowModal()
			d.Destroy()

			self.preferences.show()

		# Set up a new file
		self.rootDir = tempfile.mkdtemp(prefix="beef-")
		self.projectFilename = self.rootDir+"/config.beef"
		self.setUnsaved(False)
		self.new()

		# Load a file if given
		if file:
			self.load(file)
		else:
			self.SetStatusText("BEEF Loaded")

		self.Show(True)
	def Close(self):
		self.quit(None)
	def quit(self, event):
		if not self.confirmClose():
			return

		self.Destroy()

	def getDir(self):
		dir = os.path.dirname(os.path.realpath(__file__))
		return os.path.realpath(os.path.join(dir, ".."))
	def log(self, string):
		print(string)

	def getVersionString(self):
		return str(BEEF_VERSION_MAJOR) + "." + str(BEEF_VERSION_MINOR) + "." + str(BEEF_VERSION_PATCH)
	def ShowAbout(self):
		info = wx.adv.AboutDialogInfo()
		info.SetName("BasicEventEngine Frontend")
		info.SetVersion("v" + self.getVersionString())
		info.SetCopyright("(c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>")
		info.SetDescription(
			"BEEF is the graphical editor for the BasicEventEngine, an OpenGL game engine written in C++. "
			"The entire project is under heavy development so please report all bugs to Lou! :)"
		)
		info.SetWebSite("https://gitlab.com/piluke/BasicEventEngineFrontend")

		l = ""
		try :
			with open("LICENSE", "r") as f:
				l = f.read()
		except IOError:
			l = "LICENSE file not found, a copy is available at https://mitlicense.org/"
		info.License = l

		wx.adv.AboutBox(info)
	def savePreferences(self):
		prefFile = self.getDir()+"/preferences.json"
		with open(prefFile, "w") as f:
			f.write(json.dumps(self.prefCfg, indent=4))

	def load(self, filename):
		if not self.confirmClose():
			return

		self.ready = False
		self.projectFilename = filename

		self.log("Loading \"" + self.projectFilename + "\"...")

		self.console.clear()
		self.compiler.clean()

		self.rootDir = os.path.dirname(os.path.realpath(self.projectFilename))

		try:
			if not self._deserialize():
				return
		except IOError:
			self.log("Failed to deserialize resources during load!")
			self.SetStatusText("Failed to deserialize resources during load!")
			return

		self.treectrl.expandRoot()
		self.treectrl.SetFocus()
		self.treectrl.ClearFocusedItem()

		resources = list(itertools.chain(
			self.textures, self.sounds, self.fonts,
			self.paths, self.timelines, self.meshes,
			self.lights, self.scripts, self.objects,
			self.rooms, self.configs, self.extras
		))
		orl = self.gameCfg["open_resources"]
		self.gameCfg["open_resources"] = []
		for res in orl:
			for r in resources:
				if res == r.name:
					r.initPage()
					break

		for i in range(len(self.editDialogs)):
			self.editDialogs[i].setProgram(self.gameCfg["resource_edit_programs"][i])

		self.setUnsaved(False)
		self.SetStatusText("Loaded \"" + self.projectFilename + "\"!")
		self.ready = True
	def _deserialize(self):
		gameCfg = {}
		with open(self.rootDir+"/config.beef", "r") as f:
			gameCfg = json.loads(f.read())

		if gameCfg["beef_version_major"] < BEEF_VERSION_MAJOR or gameCfg["beef_version_minor"] < BEEF_VERSION_MINOR or gameCfg["beef_version_patch"] < BEEF_VERSION_PATCH:
			dialog = wx.MessageDialog(
				self, "This project is from an older version of BEEF, continue loading?",
				"BEEF Version Upgrade",
				wx.YES_NO | wx.ICON_QUESTION
			)

			if dialog.ShowModal() == wx.ID_NO:
				dialog.Destroy()
				self.new()
				return False

			dialog.Destroy()

			# Update BEEF version
			gameCfg["beef_version_major"] = BEEF_VERSION_MAJOR
			gameCfg["beef_version_minor"] = BEEF_VERSION_MINOR
			gameCfg["beef_version_patch"] = BEEF_VERSION_PATCH
			for k, v in self.defaultGameCfg.items():
				if k not in gameCfg:
					gameCfg[k] = v

		self.gameCfg = gameCfg
		self.gameCfg["resource_edit_programs"] = {int(k):v for k,v in self.gameCfg["resource_edit_programs"].items()} # Convert int keys from JSON strings
		for fn in glob.glob(self.rootDir+"/resources/textures/*.json"):
			with open(fn, "r") as f:
				r = BEEFTexture(self, None)
				r.deserialize(f.read())
				self.addTexture(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/sounds/*.json"):
			with open(fn, "r") as f:
				r = BEEFSound(self, None)
				r.deserialize(f.read())
				self.addSound(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/fonts/*.json"):
			with open(fn, "r") as f:
				r = BEEFFont(self, None)
				r.deserialize(f.read())
				self.addFont(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/paths/*.json"):
			with open(fn, "r") as f:
				r = BEEFPath(self, None)
				r.deserialize(f.read())
				self.addPath(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/timelines/*.json"):
			with open(fn, "r") as f:
				r = BEEFTimeline(self, None)
				r.deserialize(f.read())
				self.addTimeline(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/meshes/*.json"):
			with open(fn, "r") as f:
				r = BEEFMesh(self, None)
				r.deserialize(f.read())
				self.addMesh(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/lights/*.json"):
			with open(fn, "r") as f:
				r = BEEFLight(self, None)
				r.deserialize(f.read())
				self.addLight(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/scripts/*.json"):
			with open(fn, "r") as f:
				r = BEEFScript(self, None)
				r.deserialize(f.read())
				self.addScript(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/objects/*.json"):
			with open(fn, "r") as f:
				r = BEEFObject(self, None)
				r.deserialize(f.read())
				self.addObject(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/rooms/*.json"):
			with open(fn, "r") as f:
				r = BEEFRoom(self, None)
				r.deserialize(f.read())
				self.addRoom(r.name, r)
		for fn in glob.glob(self.rootDir+"/cfg/*"):
			with open(fn, "r") as f:
				r = BEEFConfig(self, os.path.basename(fn))
				r.deserialize(f.read())
				self.addConfig(r.name, r)
		for fn in glob.glob(self.rootDir+"/resources/extras/*"):
			with open(fn, "r") as f:
				r = BEEFExtra(self, os.path.basename(fn))
				r.deserialize(f.read())
				self.addExtra(r.name, r)
		return True

	def getUnsaved(self):
		return self._unsaved
	def setUnsaved(self, s=True):
		self._unsaved = s

		filename = self.rootDir
		if filename == "" or os.path.dirname(filename) == "/tmp":
			filename = "New"

		if self._unsaved:
			self.SetTitle("*" + os.path.basename(filename) + " - BEE Frontend v" + self.getVersionString())
		else:
			self.SetTitle(os.path.basename(filename) + " - BEE Frontend v" + self.getVersionString())
	def save(self, filename=None):
		tmpDir = ""

		if filename:
			self.projectFilename = filename

			if os.path.dirname(self.rootDir) == "/tmp":
				tmpDir = self.rootDir
			self.rootDir = os.path.dirname(self.projectFilename)

			os.mkdir(self.rootDir + "/cfg")
			os.mkdir(self.rootDir + "/resources")
			os.mkdir(self.rootDir + "/resources/extras")
			for t in EResource.getAll():
				os.mkdir(self.rootDir + "/resources/" + EResource.getPlural(t).lower())

			self.setUnsaved()

		if self.projectFilename == "":
			self.log("Failed to save, empty filename")
			return

		if os.path.dirname(self.rootDir) == "/tmp":
			self.menubar.MenuFileSaveAs(None)
			return

		self.log("Saving \"" + self.projectFilename + "\"...")

		resources = itertools.chain(
			self.textures, self.sounds, self.fonts,
			self.paths, self.timelines, self.meshes,
			self.lights, self.scripts, self.objects,
			self.rooms, self.configs, self.extras
		)
		for r in resources:
			if r:
				r.commitPage()

		for i in range(len(self.editDialogs)):
			if self.editDialogs[i].text.GetValue() == self.editDialogs[i].getDefault():
				self.gameCfg["resource_edit_programs"][i] = ""
			else:
				self.gameCfg["resource_edit_programs"][i] = self.editDialogs[i].text.GetValue()

		try:
			self._serialize()
		except IOError:
			self.log("Failed to serialize resources during save!")
			self.SetStatusText("Failed to serialize resources during save!")
			return

		if tmpDir:
			shutil.rmtree(tmpDir)

		self.setUnsaved(False)
		self.SetStatusText("Saved \"" + self.projectFilename + "\"!")
	def _serialize(self):
		with open(self.rootDir+"/config.beef", "w") as f:
			f.write(json.dumps(self.gameCfg, indent=4))

		for r in self.textures:
			if r:
				with open(self.rootDir+"/resources/textures/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.sounds:
			if r:
				with open(self.rootDir+"/resources/sounds/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.fonts:
			if r:
				with open(self.rootDir+"/resources/fonts/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.paths:
			if r:
				with open(self.rootDir+"/resources/paths/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.timelines:
			if r:
				with open(self.rootDir+"/resources/timelines/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.meshes:
			if r:
				with open(self.rootDir+"/resources/meshes/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.lights:
			if r:
				with open(self.rootDir+"/resources/lights/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.scripts:
			if r:
				with open(self.rootDir+"/resources/scripts/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.objects:
			if r:
				with open(self.rootDir+"/resources/objects/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.rooms:
			if r:
				with open(self.rootDir+"/resources/rooms/" + r.name + ".json", "w") as f:
					f.write(r.serialize())
		for r in self.configs:
			if r:
				with open(self.rootDir+"/cfg/" + r.name, "w") as f:
					f.write(r.serialize())
		for r in self.extras:
			if r:
				with open(self.rootDir+"/resources/extras/" + r.name, "w") as f:
					d = r.serialize()
					if not d is None:
						f.write(d)
	def confirmOverwriteResource(self, fn, name):
		dialog = wx.MessageDialog(
			self, "Another resource has the name \"" + name + "\". Overwrite it?",
			"Resource Name Conflict",
			wx.YES_NO | wx.ICON_QUESTION
		)

		if dialog.ShowModal() == wx.ID_NO:
			dialog.Destroy()
			return False

		dialog.Destroy()
		return True
	def dialogRename(self, name, is_conflict=True, title=None, msg=None):
		if name and is_conflict:
			title = "Name Conflict"
			msg = "Another resource has the name \"" + name + "\".\nPlease enter a different name for the selected resource:"
		if not title:
			title = "Resource Naming"
		if not msg:
			msg = "Choose a name for the new resource:"

		dialog = wx.TextEntryDialog(self, msg, title)
		dialog.SetValue(name)

		v = None
		if dialog.ShowModal() == wx.ID_OK:
			v = dialog.GetValue()
		dialog.Destroy()

		return v

	def confirmClose(self):
		if self.getUnsaved():
			dialog = wx.MessageDialog(
				self, "Save before closing?",
				"Unsaved Changes",
				wx.YES_NO | wx.CANCEL | wx.ICON_QUESTION
			)
			v = dialog.ShowModal()

			if v == wx.ID_YES:
				self.menubar.MenuFileSave(None)
				if self.getUnsaved():
					dialog.Destroy()
					return False
			elif v == wx.ID_NO:
				pass
			else:
				dialog.Destroy()
				return False
			dialog.Destroy()

		self.projectFilename = ""

		old_res = self.notebook.resources.copy()
		for (p, r) in old_res.items():
			if r:
				r.destroyPage(isClosing=True)
		while self.notebook.GetPageCount() > 0:
			page = self.notebook.GetPage(0)
			self.notebook.RemovePage(0)
			page.DestroyLater()

		self.textures = []
		self.sounds = []
		self.fonts = []
		self.paths = []
		self.timelines = []
		self.meshes = []
		self.lights = []
		self.scripts = []
		self.objects = []
		self.rooms = []
		self.configs = []
		self.extras = []

		self.treectrl.reset()

		if os.path.dirname(self.rootDir) == "/tmp":
			shutil.rmtree(self.rootDir)
		self.rootDir = ""

		self.SetStatusText("Project closed")
		return True
	def new(self):
		self.compiler.clean()

		if os.path.dirname(self.rootDir) != "/tmp":
			self.rootDir = tempfile.mkdtemp(prefix="beef-")
		self.projectFilename = self.rootDir+"/config.beef"

		self.gameCfg = self.defaultGameCfg
		with open(self.projectFilename, "w") as f:
			f.write(json.dumps(self.gameCfg, indent=4))

		os.mkdir(self.rootDir + "/cfg")
		os.mkdir(self.rootDir + "/resources")
		os.mkdir(self.rootDir + "/resources/extras")
		for t in EResource.getAll():
			os.mkdir(self.rootDir + "/resources/" + EResource.getPlural(t).lower())

		with open("templates/config.py") as configFile:
			self.addConfig("config.py")[0].properties["content"] = configFile.read()
		with open("templates/binds.py") as bindsFile:
			self.addConfig("binds.py")[0].properties["content"] = bindsFile.read()

		self.setUnsaved(False)
		self.SetTitle("New - BEE Frontend v" + self.getVersionString())
		self.SetStatusText("New project created")
		self.ready = True

	def getNextGid(self):
		return (
			len(self.textures) +
			len(self.sounds) +
			len(self.fonts) +
			len(self.paths) +
			len(self.timelines) +
			len(self.meshes) +
			len(self.lights) +
			len(self.scripts) +
			len(self.objects) +
			len(self.rooms)
		)
	def addTexture(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFTexture(self, name)

		r.id = len(self.textures)
		r.gid = self.getNextGid()
		r.resourceList = self.textures

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.textures.append(r)
		i = self.treectrl.addTexture(name, r)
		r.treeitem = i

		return (r, i)
	def addSound(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFSound(self, name)

		r.id = len(self.sounds)
		r.gid = self.getNextGid()
		r.resourceList = self.sounds

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.sounds.append(r)
		i = self.treectrl.addSound(name, r)
		r.treeitem = i

		return (r, i)
	def addFont(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFFont(self, name)

		r.id = len(self.fonts)
		r.gid = self.getNextGid()
		r.resourceList = self.fonts

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.fonts.append(r)
		i = self.treectrl.addFont(name, r)
		r.treeitem = i

		return (r, i)
	def addPath(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFPath(self, name)

		r.id = len(self.paths)
		r.gid = self.getNextGid()
		r.resourceList = self.paths

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.paths.append(r)
		i = self.treectrl.addPath(name, r)
		r.treeitem = i

		return (r, i)
	def addTimeline(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFTimeline(self, name)

		r.id = len(self.timelines)
		r.gid = self.getNextGid()
		r.resourceList = self.timelines

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.timelines.append(r)
		i = self.treectrl.addTimeline(name, r)
		r.treeitem = i

		return (r, i)
	def addMesh(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFMesh(self, name)

		r.id = len(self.meshes)
		r.gid = self.getNextGid()
		r.resourceList = self.meshes

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.meshes.append(r)
		i = self.treectrl.addMesh(name, r)
		r.treeitem = i

		return (r, i)
	def addLight(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFLight(self, name)

		r.id = len(self.lights)
		r.gid = self.getNextGid()
		r.resourceList = self.lights

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.lights.append(r)
		i = self.treectrl.addLight(name, r)
		r.treeitem = i

		return (r, i)
	def addScript(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFScript(self, name)

		r.id = len(self.scripts)
		r.gid = self.getNextGid()
		r.resourceList = self.scripts

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.scripts.append(r)
		i = self.treectrl.addScript(name, r)
		r.treeitem = i

		return (r, i)
	def addObject(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFObject(self, name)

		r.id = len(self.objects)
		r.gid = self.getNextGid()
		r.resourceList = self.objects

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.objects.append(r)
		i = self.treectrl.addObject(name, r)
		r.treeitem = i

		return (r, i)
	def addRoom(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFRoom(self, name)

		r.id = len(self.rooms)
		r.gid = self.getNextGid()
		r.resourceList = self.rooms

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.rooms.append(r)
		i = self.treectrl.addRoom(name, r)
		r.treeitem = i

		return (r, i)
	def addConfig(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFConfig(self, name)

		r.id = len(self.configs)
		r.resourceList = self.configs

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.configs.append(r)
		i = self.treectrl.addConfig(name, r)
		r.treeitem = i

		return (r, i)
	def addExtra(self, name, resource=None):
		r = resource
		if not r:
			r = BEEFExtra(self, name)

		r.id = len(self.extras)
		r.resourceList = self.extras

		while not r.checkName(name, shouldDelete=False):
			name = self.dialogRename(name)

			if not name:
				return (None, None)

		self.setUnsaved()
		r.name = name

		self.extras.append(r)
		i = self.treectrl.addExtra(name, r)
		r.treeitem = i

		return (r, i)

	def getResourceFromPath(self, path):
		roots = [
			"/resources/textures/",
			"/resources/sounds/",
			"/resources/fonts/",
			"/resources/paths/",
			"/resources/timelines/",
			"/resources/meshes/",
			"/resources/lights/",
			"/resources/scripts/",
			"/resources/objects/",
			"/resources/rooms/",
			"/cfg/",
			"/resources/extras/"
		]
		rlists = [
			self.textures,
			self.sounds,
			self.fonts,
			self.paths,
			self.timelines,
			self.meshes,
			self.lights,
			self.scripts,
			self.objects,
			self.rooms,
			self.configs,
			self.extras
		]

		i = 0
		for root in roots:
			if path.startswith(root):
				for r in rlists[i]:
					if path == root+r.name+".json" or ("path" in r.properties and path == r.properties["path"]):
						return rlists[i][r.id]
				else:
					return None
			i += 1

		return None
	def editResource(self, resource):
		p = self.editDialogs[resource.type].show()
		if p:
			if resource.properties["path"]:
				subprocess.Popen([p, self.rootDir+resource.path+resource.properties["path"]])
			else:
				subprocess.Popen([p])
			return True

		return False

if __name__ == "__main__":
	file = None
	if len(sys.argv) > 1:
		file = sys.argv[1]

	app = wx.App()
	frame = BEEFFrame(None, -1, file)
	app.MainLoop()
