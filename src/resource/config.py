# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import copy
import os
import shutil

from resource.base import BEEFBaseResource
from resource.enum import EResource

class BEEFConfig(BEEFBaseResource):
	def __init__(self, top, name):
		BEEFBaseResource.__init__(self, top, name)
		self.path = "/cfg/"
		self.type = EResource.CONFIG
		self.properties = {
			"content": ""
		}

	def initPageSpecific(self):
		self.gbs = wx.GridBagSizer(12, 2)

		# Column 1
		self.pageAddStatictext("Name:", (0,0))
		self.pageAddTextctrl("tc_name", self.name, (1,0), (1,2))
		self.pageAddButton("bt_import", "Import", (2,0))

		self.pageAddButton("bt_ok", "OK", (3,0))

		# Column 2
		ed = self.pageAddEditor("ed_content", (0,3), (6,1))
		ed.SetText(self.properties["content"])
		ed.EmptyUndoBuffer()

		self.gbs.AddGrowableRow(2)
		self.gbs.AddGrowableCol(3)
		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.page.SetSizer(self.sizer)

	def focusPageSpecific(self):
		if "ed_content" in self.inputs:
			self.inputs["ed_content"].SetFocus()

	def onTextSpecific(self, event):
		return True
	def onButtonSpecific(self, event):
		bt = event.GetEventObject()
		if bt == self.inputs["bt_ok"]:
			self.destroyPage()
		elif bt == self.inputs["bt_import"]:
			wildcards = (
				"Config File (*.cfg)|*.cfg|"
				"All files (*)|*"
			)

			d = self.top.rootDir+os.path.dirname(self.path+self.name)
			f = os.path.basename(self.path+self.name)

			dialog = wx.FileDialog(
				self.top, message="Import Config File",
				defaultDir=d,
				defaultFile=f,
				wildcard=wildcards,
				style=wx.FD_OPEN
			)

			if dialog.ShowModal() == wx.ID_OK:
				path = dialog.GetPath()
				ext = os.path.splitext(path)[1]

				p = self.top.rootDir+self.path+self.properties["path"]
				if path != p:
					shutil.copyfile(path, p)

				self.update()
			else:
				dialog.Destroy()
				return False

			dialog.Destroy()

		return True
	def onEditorSpecific(self, event):
		return True

	def serialize(self):
		return self.properties["content"]
	def deserialize(self, data):
		self.properties["content"] = data

	def update(self, res=None):
		p = {k : v for k, v in self.properties.items()}
		if res:
			BEEFBaseResource.update(self, res)
			self.inputs["tc_name"].SetValue(self.name)

			p["content"] = res.inputs["ed_content"].GetText()
		else:
			with open(self.top.rootDir+self.path+self.name, "r") as f:
				p["content"] = f.read()

		self.inputs["ed_content"].UpdateText(p["content"])

	def commitPage(self):
		if self.page:
			tc_name = self.inputs["tc_name"]
			if tc_name.GetValue() != self.name:
				self.rename(tc_name.GetValue())

			self.properties["content"] = self.inputs["ed_content"].GetText()
	def rename(self, name):
		if name == self.name:
			return True

		if not self.checkName(name):
			return False

		oldfile = self.top.rootDir+self.path+self.name
		newfile = self.top.rootDir+self.path+name
		if os.path.isfile(oldfile):
			os.rename(oldfile, newfile)

		if self.name in self.top.gameCfg["open_resources"]:
			self.top.gameCfg["open_resources"].remove(self.name)
			self.top.gameCfg["open_resources"].append(name)

		self.name = name

		if self.treeitem:
			self.top.treectrl.SetItemText(self.treeitem, self.name)

		if self.page:
			self.top.notebook.SetPageText(self.pageIndex, self.name)

		self.top.setUnsaved()
		return True

	def MenuDuplicate(self, event):
		r = BEEFConfig(self.top, None)
		r.properties["content"] = self.properties["content"]
		self.top.addConfig(self.name, r)
