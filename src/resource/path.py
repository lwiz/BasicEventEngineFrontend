# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import copy
import os
import shutil

from resource.base import BEEFBaseResource
from resource.enum import EResource

from ui.validators import BEEFValidatorFloat

class BEEFPath(BEEFBaseResource):
	def __init__(self, top, name):
		BEEFBaseResource.__init__(self, top, name)
		self.path = "/resources/paths/"
		self.type = EResource.PATH
		self.properties = {
			"nodes": [], # Each node is in the format ((x, y, z), speed)
			"control_points": {} # Each control point is in the format (node: (x, y, z))
		}

	def getRes(self):
		res = "\t\t\t{\n"
		props = [
			"\"name\": \"{}\"".format(self.name),
			"\"path\": \"$/{}.nodes.json\"".format(self.name)
		]
		res += "\t\t\t\t" + ",\n\t\t\t\t".join(props)
		res += "\n\t\t\t}"
		return res

	def addNodeRows(self, nodes, cps):
		self.inputs["lst_nodes"].DeleteAllItems()
		i = 0
		for n in nodes:
			x, y, z = n[0]
			s = n[1]
			cx = cy = cz = str(float("nan"))
			if str(i) in cps:
				cx, cy, cz = cps[str(i)]

			self.addListRow("lst_nodes", [x, y, z, s, cx, cy, cz])

			i += 1
	def getNodeRows(self):
		nodes = []
		cps = {}
		i = 0
		for n in self.inputs["lst_nodes"].GetItemList():
			nodes.append(((n[0], n[1], n[2]), n[3]))
			cps[str(i)] = (n[4], n[5], n[6])
			i += 1
		return (nodes, cps)
	def initPageSpecific(self):
		self.gbs = wx.GridBagSizer(12, 2)

		# Column 1
		self.pageAddStatictext("Name:", (0,0))
		self.pageAddTextctrl("tc_name", self.name, (1,0), (1,2))

		lst = self.pageAddListCtrl("lst_nodes", ["X", "Y", "Z", "Speed", "CP X", "CP Y", "CP Z"], (2,0), (1,2)) # Perhaps replace with a Grid
		lst.SetValidators(BEEFValidatorFloat())
		self.addNodeRows(self.properties["nodes"], self.properties["control_points"])

		self.pageAddButton("bt_add_node", "Add Node", (3,0))
		self.pageAddButton("bt_remove_node", "Remove Node", (3,1))

		self.pageAddButton("bt_ok", "OK", (4,0))

		# Column 2
		pl = self.pageAddPlot("pl_nodes", (0,3), (4,1))
		pl.SetCoords(self.inputs["lst_nodes"].GetItemList())
		pl.Redraw()

		self.gbs.AddGrowableRow(2)
		self.gbs.AddGrowableCol(3)
		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.page.SetSizer(self.sizer)

	def onTextSpecific(self, event):
		return True
	def onButtonSpecific(self, event):
		bt = event.GetEventObject()
		if bt == self.inputs["bt_ok"]:
			self.destroyPage()
		elif bt == self.inputs["bt_add_node"]:
			self.addListRow("lst_nodes", ["0", "0", "0", "1.0", float("nan"), float("nan"), float("nan")])
			pl = self.inputs["pl_nodes"]
			pl.SetCoords(self.inputs["lst_nodes"].GetItemList())
			pl.Redraw()
		elif bt == self.inputs["bt_remove_node"]:
			self.removeListRow("lst_nodes", -1)
			pl = self.inputs["pl_nodes"]
			pl.SetCoords(self.inputs["lst_nodes"].GetItemList())
			pl.Redraw()

		return True
	def onListEditSpecific(self, event):
		pl = self.inputs["pl_nodes"]

		# Update the nodes with the new event data
		nodes = self.inputs["lst_nodes"].GetItemList()
		item = event.GetItem()
		n = list(nodes[item.GetId()])
		n[item.GetColumn()] = item.GetText()
		nodes[item.GetId()] = tuple(n)
		pl.SetCoords(nodes)

		pl.Redraw()
		return True

	def update(self, res=None):
		p = {k : v for k, v in self.properties.items()}
		if res:
			BEEFBaseResource.update(self, res)
			self.inputs["tc_name"].SetValue(self.name)

			nodes, cps = res.getNodeRows()
			p["nodes"] = nodes
			p["control_points"] = cps

		self.addNodeRows(p["nodes"], p["control_points"])
		pl = self.inputs["pl_nodes"]
		pl.SetCoords(self.inputs["lst_nodes"].GetItemList())
		pl.Redraw()

	def commitPage(self):
		if self.page:
			tc_name = self.inputs["tc_name"]
			if tc_name.GetValue() != self.name:
				self.rename(tc_name.GetValue())

			nodes, cps = self.getNodeRows()
			self.properties["nodes"] = nodes
			self.properties["control_points"] = cps
	def moveTo(self, name, newfile):
		"""if self.properties["path"]:
			ext = os.path.splitext(self.properties["path"])[1]
			os.rename(self.top.rootDir+self.properties["path"], newfile+ext)
			self.properties["path"] = self.path+name+ext
			self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))"""

	def MenuDuplicate(self, event):
		r = BEEFPath(self.top, None)
		r.properties = copy.deepcopy(self.properties)
		self.top.addPath(self.name, r)
