# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import copy
import os
import shutil

from resource.base import BEEFBaseResource
from resource.enum import EResource
from resource.enum import EEvent

from ui.eventdialog import BEEFEventDialog

class BEEFScript(BEEFBaseResource):
	def __init__(self, top, name):
		BEEFBaseResource.__init__(self, top, name)
		self.path = "/resources/scripts/"
		self.type = EResource.SCRIPT
		self.properties = {
			"code": "import bee\n"
		}

	def getRes(self):
		res = "\t\t\t{\n"
		props = [
			"\"name\": \"{}\"".format(self.name),
			"\"path\": \"$/{}.py\"".format(self.name)
		]
		res += "\t\t\t\t" + ",\n\t\t\t\t".join(props)
		res += "\n\t\t\t}"
		return res

	def initPageSpecific(self):
		self.gbs = wx.GridBagSizer(12, 2)

		# Column 1
		self.pageAddStatictext("Name:", (0,0))
		self.pageAddTextctrl("tc_name", self.name, (1,0), (1,2))

		self.pageAddButton("bt_ok", "OK", (2,0))

		# Column 2
		self.lastSelected = -1
		ed = self.pageAddEditor("ed_code", (0,3), (3,1))
		ed.SetText(self.properties["code"])
		ed.EmptyUndoBuffer()

		self.gbs.AddGrowableRow(2)
		self.gbs.AddGrowableCol(3)
		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.page.SetSizer(self.sizer)

	def focusPageSpecific(self):
		if "ed_code" in self.inputs:
			self.inputs["ed_code"].SetFocus()

	def onTextSpecific(self, event):
		tc = event.GetEventObject()
		if tc == self.inputs["tc_name"]:
			pass # TODO: Handle name changes within the user's code
		return True
	def onButtonSpecific(self, event):
		bt = event.GetEventObject()
		if bt == self.inputs["bt_ok"]:
			self.destroyPage()
		return True
	def onEditorSpecific(self, event):
		return True

	def update(self, res=None):
		p = {k : v for k, v in self.properties.items()}
		if res:
			BEEFBaseResource.update(self, res)
			self.inputs["tc_name"].SetValue(self.name)

			p["code"] = res.inputs["ed_code"].GetText()

		self.inputs["ed_code"].UpdateText(p["code"])

	def commitPage(self):
		if self.page:
			tc_name = self.inputs["tc_name"]
			if tc_name.GetValue() != self.name:
				self.rename(tc_name.GetValue())

			self.properties["code"] = self.inputs["ed_code"].GetText()
	def moveTo(self, name, newfile):
		"""if self.properties["path"]:
			ext = os.path.splitext(self.properties["path"])[1]
			os.rename(self.top.rootDir+self.properties["path"], newfile+ext)
			self.properties["path"] = self.path+name+ext
			self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))"""

	def MenuDuplicate(self, event):
		r = BEEFScript(self.top, None)
		r.properties = copy.deepcopy(self.properties)
		self.top.addScript(self.name, r)
