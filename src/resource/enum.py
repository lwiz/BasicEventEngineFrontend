# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

class EResource:
	TEXTURE = 0
	SOUND = 1
	FONT = 2
	PATH = 3
	TIMELINE = 4
	MESH = 5
	LIGHT = 6
	SCRIPT = 7
	OBJECT = 8
	ROOM = 9
	_MAX = 10

	CONFIG = 11
	EXTRA = 12

	@staticmethod
	def get(type):
		if type == EResource.TEXTURE:
			return "Texture"
		elif type == EResource.SOUND:
			return "Sound"
		elif type == EResource.FONT:
			return "Font"
		elif type == EResource.PATH:
			return "Path"
		elif type == EResource.TIMELINE:
			return "Timeline"
		elif type == EResource.MESH:
			return "Mesh"
		elif type == EResource.LIGHT:
			return "Light"
		elif type == EResource.SCRIPT:
			return "Script"
		elif type == EResource.OBJECT:
			return "Object"
		elif type == EResource.ROOM:
			return "Room"
		elif type == EResource.CONFIG:
			return "Config File"
		elif type == EResource.EXTRA:
			return "Extra File"
	@staticmethod
	def getPlural(type):
		if type == EResource.MESH:
			return "Meshes"
		else:
			return EResource.get(type) + "s"
	@staticmethod
	def getAll():
		return range(EResource._MAX)

class EFontStyle:
	NORMAL = 0
	BOLD = 1
	ITALIC = 2
	UNDERLINE = 3
	STRIKETHROUGH = 4
	_MAX = 5

	@staticmethod
	def str(type):
		if type == EFontStyle.NORMAL:
			return "NORMAL"
		elif type == EFontStyle.BOLD:
			return "BOLD"
		elif type == EFontStyle.ITALIC:
			return "ITALIC"
		elif type == EFontStyle.UNDERLINE:
			return "UNDERLINE"
		elif type == EFontStyle.STRIKETHROUGH:
			return "STRIKETHROUGH"
		return "NONE"

class ELight:
	AMBIENT = 1
	DIFFUSE = 2
	POINT = 3
	SPOT = 4
	_MAX = 5

	@staticmethod
	def str(type):
		if type == ELight.AMBIENT:
			return "AMBIENT"
		elif type == ELight.DIFFUSE:
			return "DIFFUSE"
		elif type == ELight.POINT:
			return "POINT"
		elif type == ELight.SPOT:
			return "SPOT"
		return "NONE"

class ECompile:
	DEBUG = 0
	RELEASE = 1

class EEvent:
	_events = [
		"Update",
		"Create",
		"Destroy",
		"Alarm",
		"Step Begin",
		"Step Mid",
		"Step End",
		"Keyboard Press",
		"Mouse Press",
		"Keyboard Input",
		"Mouse Input",
		"Keyboard Release",
		"Mouse Release",
		"Controller Axis",
		"Controller Press",
		"Controller Release",
		"Controller Modify",
		"Commandline Input",
		"Path End",
		"Outside Room",
		"Intersect Boundary",
		"Collision",
		"Check Collision Filter",
		"Draw",
		"Animation End",
		"Room Start",
		"Room End",
		"Game Start",
		"Game End",
		"Window",
		"Network",
		"IO"
	]
	@staticmethod
	def get(index):
		return EEvent._events[index]
	@staticmethod
	def getIndex(event):
		return EEvent._events.index(event)
	@staticmethod
	def getParams(event):
		if event == "Update":
			return "self"
		elif event == "Create":
			return "self"
		elif event == "Destroy":
			return "self"
		elif event == "Alarm":
			return "self, alarm"
		elif event == "Step Begin":
			return "self"
		elif event == "Step Mid":
			return "self"
		elif event == "Step End":
			return "self"
		elif event == "Keyboard Press":
			return "self, e"
		elif event == "Mouse Press":
			return "self, e"
		elif event == "Keyboard Input":
			return "self, e"
		elif event == "Mouse Input":
			return "self, e"
		elif event == "Keyboard Release":
			return "self, e"
		elif event == "Mouse Release":
			return "self, e"
		elif event == "Controller Axis":
			return "self, e"
		elif event == "Controller Press":
			return "self, e"
		elif event == "Controller Release":
			return "self, e"
		elif event == "Controller Modify":
			return "self, e"
		elif event == "Commandline Input":
			return "self, input"
		elif event == "Path End":
			return "self"
		elif event == "Outside Room":
			return "self"
		elif event == "Intersect Boundary":
			return "self"
		elif event == "Collision":
			return "self, other"
		elif event == "Check Collision Filter":
			return "self, other"
		elif event == "Draw":
			return "self"
		elif event == "Animation End":
			return "self"
		elif event == "Room Start":
			return "self"
		elif event == "Room End":
			return "self"
		elif event == "Game Start":
			return "self"
		elif event == "Game End":
			return "self"
		elif event == "Window":
			return "self, e"
		elif event == "Network":
			return "self, e"
		elif event == "IO":
			return "self, data"
	@staticmethod
	def getParamTypes(event):
		params = EEvent.getParams(event)
		params = params.split(",")
		return ", ".join([p.rpartition(" ")[0] for p in params])
