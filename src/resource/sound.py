# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import wx.adv

import copy
import os
import shutil

from resource.base import BEEFBaseResource
from resource.enum import EResource

class BEEFSound(BEEFBaseResource):
	def __init__(self, top, name):
		BEEFBaseResource.__init__(self, top, name)
		self.path = "/resources/sounds/"
		self.type = EResource.SOUND
		self.sound = None
		self.properties = {
			"path": "",
			"is_music": False,
			"volume": 100.0,
			"pan": 0.0,
			"effects": {} # Each effect is as follows: {"type": E_SOUNDEFFECT, "params": {}}
		}

	def getRes(self):
		res = "\t\t\t{\n"
		props = [
			"\"name\": \"{}\"".format(self.name),
			"\"path\": \"$/{}\"".format(self.properties["path"]),
			"\"is_music\": {}".format("true" if self.properties["is_music"] else "false"),
			"\"volume\": {}".format(self.properties["volume"] / 100),
			"\"pan\": {}".format(self.properties["pan"] / 100),
			"\"effects\": {}".format(self.properties["effects"])
		]
		res += "\t\t\t\t" + ",\n\t\t\t\t".join(props)
		res += "\n\t\t\t}"
		return res

	def initPageSpecific(self):
		self.gbs = wx.GridBagSizer(12, 2)

		self.pageAddStatictext("Name:", (0,0))
		self.pageAddTextctrl("tc_name", self.name, (1,0), (1,2))
		self.pageAddButton("bt_edit", "Edit Sound", (2,0))
		self.pageAddButton("bt_import", "Import", (2,1))

		self.pageAddStatictext("Volume:", (3,0))
		self.pageAddSlider("sl_volume", self.properties["volume"], (4,0))

		path = self.properties["path"]
		if path:
			self.sound = wx.adv.Sound(self.top.rootDir+self.path+path)

		playSizer = wx.BoxSizer()
		bt_play = self.pageMakeBmpbutton("bt_play", "images/sound/play.png", tooltip="Play Sound")
		playSizer.Add(bt_play, 1, wx.ALL | wx.EXPAND, 0)
		bt_loop = self.pageMakeBmpbutton("bt_loop", "images/sound/loop.png", tooltip="Loop Sound")
		playSizer.Add(bt_loop, 1, wx.ALL | wx.EXPAND, 0)
		bt_stop = self.pageMakeBmpbutton("bt_stop", "images/sound/stop.png", tooltip="Stop Sound")
		playSizer.Add(bt_stop, 1, wx.ALL | wx.EXPAND, 0)
		self.gbs.Add(playSizer, (4,1), (1,1))

		self.pageAddStatictext("Pan:", (5,0))
		self.pageAddSlider("sl_pan", self.properties["pan"], (6,0), minvalue=-100, maxvalue=100)

		self.pageAddStatictext("Path: {}".format(path), (7,0), name="st_path")

		self.pageAddButton("bt_ok", "OK", (8,0))

		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.page.SetSizer(self.sizer)

	def onTextSpecific(self, event):
		return True
	def onButtonSpecific(self, event):
		bt = event.GetEventObject()
		if bt == self.inputs["bt_ok"]:
			self.destroyPage()
		elif bt == self.inputs["bt_edit"]:
			return self.top.editResource(self)
		elif bt == self.inputs["bt_import"]:
			wildcards = (
				"WAV Sound (*.wav)|*.wav|"
				"All files (*)|*"
			)

			d = self.top.rootDir+self.path+os.path.dirname(self.properties["path"])
			f = os.path.basename(self.properties["path"])
			if not self.properties["path"]:
				d = os.getcwd()
				f = ""

			dialog = wx.FileDialog(
				self.top, message="Import Sound",
				defaultDir=d,
				defaultFile=f,
				wildcard=wildcards,
				style=wx.FD_OPEN
			)

			if dialog.ShowModal() == wx.ID_OK:
				path = dialog.GetPath()
				ext = os.path.splitext(path)[1]

				self.properties["path"] = self.name+ext
				p = self.top.rootDir+self.path+self.properties["path"]
				if path != p:
					shutil.copyfile(path, p)

				self.update()
			else:
				dialog.Destroy()
				return False

			dialog.Destroy()
		elif bt == self.inputs["bt_play"]:
			if self.sound and self.sound.IsOk():
				self.sound.Play(wx.adv.SOUND_ASYNC)
			return False
		elif bt == self.inputs["bt_loop"]:
			if self.sound and self.sound.IsOk():
				self.sound.Play(wx.adv.SOUND_ASYNC | wx.adv.SOUND_LOOP)
			return False
		elif bt == self.inputs["bt_stop"]:
			if self.sound and self.sound.IsOk():
				self.sound.Stop()
			return False

		return True
	def onSliderSpecific(self, event):
		return True

	def update(self, res=None):
		p = {k : v for k, v in self.properties.items()}
		if res:
			BEEFBaseResource.update(self, res)
			self.inputs["tc_name"].SetValue(self.name)

			p["volume"] = res.inputs["sl_volume"].GetValue()
			p["pan"] = res.inputs["sl_pan"].GetValue()

		self.sound = wx.adv.Sound(self.top.rootDir+self.path+self.properties["path"])
		self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))

		self.inputs["sl_volume"].SetValue(p["volume"])
		self.inputs["sl_pan"].SetValue(p["pan"])

	def commitPage(self):
		if self.page:
			tc_name = self.inputs["tc_name"]
			if tc_name.GetValue() != self.name:
				self.rename(tc_name.GetValue())

			self.properties["volume"] = self.inputs["sl_volume"].GetValue()
			self.properties["pan"] = self.inputs["sl_pan"].GetValue()
	def moveTo(self, name, newfile):
		ext = os.path.splitext(self.properties["path"])[1]
		os.rename(self.top.rootDir+self.path+self.properties["path"], newfile+ext)
		self.properties["path"] = name+ext
		self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))

	def MenuDuplicate(self, event):
		r = BEEFSound(self.top, None)
		r.properties = copy.deepcopy(self.properties)
		self.top.addSound(self.name, r)
