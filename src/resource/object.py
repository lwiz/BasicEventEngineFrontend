# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import copy
import os
import shutil

from resource.base import BEEFBaseResource
from resource.enum import EResource
from resource.enum import EEvent

from ui.eventdialog import BEEFEventDialog

class BEEFObject(BEEFBaseResource):
	initialEventCode = "def {event}({params}):\n\tpass\n"
	def __init__(self, top, name):
		BEEFBaseResource.__init__(self, top, name)
		self.path = "/resources/objects/"
		self.type = EResource.OBJECT
		self.properties = {
			"sprite": "",
			"is_persistent": False,
			"depth": 0,
			"parent": "",
			"mask_offset": (0, 0),
			"is_pausable": True,
			"code": "import bee\n\n{}".format(self.getInitialCode("Create"))
		}

	def getNewEvent(self):
		newEvent = ""

		dialog = BEEFEventDialog(self.page)
		if dialog.ShowModal() == wx.ID_OK:
			newEvent = dialog.getEventStr()

		dialog.Destroy()
		return newEvent
	def getInitialCode(self, event):
		if not self.name:
			return ""

		return self.initialEventCode.format(event=event.lower().replace(" ", "_"), params=EEvent.getParams(event))

	def getImplementedEvents(self):
		return ["bee::E_EVENT::{}".format(EEvent.get(event).upper().replace(" ", "_")) for event, data in self.tmpEvents.items() if EEvent.get(event) not in ["Extra Headers", "Extra Functions"]]
	def getEvents(self):
		return [data[1] for event, data in self.tmpEvents.items() if EEvent.get(event) != "Extra Headers"]
	def getRes(self):
		res = "\t\t\t{\n"
		props = [
			"\"name\": \"{}\"".format(self.name),
			"\"path\": \"$/{}.py\"".format(self.name),
			"\"sprite\": \"{}\"".format(self.properties["sprite"]),
			"\"is_persistent\": {}".format("true" if self.properties["is_persistent"] else "false"),
			"\"depth\": {}".format(self.properties["depth"]),
			"\"parent\": \"{}\"".format(self.properties["parent"]),
			"\"mask_offset\": [{}, {}]".format(*self.properties["mask_offset"]),
			"\"is_pausable\": {}".format("true" if self.properties["is_pausable"] else "false")
		]
		res += "\t\t\t\t" + ",\n\t\t\t\t".join(props)
		res += "\n\t\t\t}"
		return res

	def initPageSpecific(self):
		self.gbs = wx.GridBagSizer(12, 2)

		# Column 1
		self.pageAddStatictext("Name:", (0,0))
		self.pageAddTextctrl("tc_name", self.name, (1,0), (1,2))

		self.pageAddStatictext("Sprite:", (2,0))
		spriteList = [""] + [s.name for s in self.top.textures]
		spriteIndex = -1
		try:
			spriteIndex = spriteList.index(self.properties["sprite"])
		except ValueError:
			pass
		self.pageAddChoice("ch_sprite", spriteList, spriteIndex, (2,1))

		self.pageAddCheckbox("cb_is_persistent", "Persistent", (3,0), self.properties["is_persistent"])

		self.pageAddButton("bt_add_event", "Add Event", (4,0))

		self.pageAddButton("bt_ok", "OK", (5,0))

		# Column 2
		self.lastSelected = -1
		ed = self.pageAddEditor("ed_code", (0,3), (5,1))
		ed.SetText(self.properties["code"])
		ed.EmptyUndoBuffer()

		self.gbs.AddGrowableRow(4)
		self.gbs.AddGrowableCol(3)
		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.page.SetSizer(self.sizer)

	def focusPageSpecific(self):
		if "ed_code" in self.inputs:
			self.inputs["ed_code"].SetFocus()

	def onTextSpecific(self, event):
		tc = event.GetEventObject()
		if tc == self.inputs["tc_name"]:
			pass # TODO: Handle name changes within the user's code
		return True
	def onCheckBoxSpecific(self, event):
		return True
	def onButtonSpecific(self, event):
		bt = event.GetEventObject()
		if bt == self.inputs["bt_ok"]:
			self.destroyPage()
		elif bt == self.inputs["bt_add_event"]:
			newEvent = self.getNewEvent()
			if not newEvent:
				return False

			initialCode = self.getInitialCode(newEvent)

			ed = self.inputs["ed_code"]
			ed.SetText(ed.GetText() + "\n" + initialCode)
			ed.SetFocus()
			ed.GotoPos(len(ed.GetText())-1)

		return True
	def onChoiceSpecific(self, event):
		return True
	def onEditorSpecific(self, event):
		return True

	def update(self, res=None):
		p = {k : v for k, v in self.properties.items()}
		if res:
			BEEFBaseResource.update(self, res)
			self.inputs["tc_name"].SetValue(self.name)

			p["sprite"] = res.inputs["ch_sprite"].GetSelection()
			p["is_persistent"] = res.inputs["cb_is_persistent"].GetValue()
			p["code"] = res.inputs["ed_code"].GetText()

		self.inputs["ch_sprite"].SetSelection(p["sprite"])
		self.inputs["cb_is_persistent"].SetValue(p["is_persistent"])

		self.inputs["ed_code"].UpdateText(p["code"])

	def commitPage(self):
		if self.page:
			tc_name = self.inputs["tc_name"]
			if tc_name.GetValue() != self.name:
				self.rename(tc_name.GetValue())

			ch = self.inputs["ch_sprite"]
			self.properties["sprite"] = ch.GetString(ch.GetSelection())
			self.properties["is_persistent"] = self.inputs["cb_is_persistent"].GetValue()

			self.properties["code"] = self.inputs["ed_code"].GetText()
	def moveTo(self, name, newfile):
		"""if self.properties["path"]:
			ext = os.path.splitext(self.properties["path"])[1]
			os.rename(self.top.rootDir+self.properties["path"], newfile+ext)
			self.properties["path"] = self.path+name+ext
			self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))"""

	def MenuDuplicate(self, event):
		r = BEEFObject(self.top, None)
		r.properties = copy.deepcopy(self.properties)
		self.top.addObject(self.name, r)
