# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import copy
import os
import shutil

from resource.base import BEEFBaseResource
from resource.enum import EResource

class BEEFMesh(BEEFBaseResource):
	def __init__(self, top, name):
		BEEFBaseResource.__init__(self, top, name)
		self.path = "/resources/meshes/"
		self.type = EResource.MESH
		self.properties = {
			"path": "",
			"has_material": False,
			"mesh_index": 0
		}

	def getRes(self):
		res = "\t\t\t{\n"
		props = [
			"\"name\": \"{}\"".format(self.name),
			"\"path\": \"$/{}\"".format(self.properties["path"]),
			"\"mesh_index\": {}".format(self.properties["mesh_index"])
		]
		res += "\t\t\t\t" + ",\n\t\t\t\t".join(props)
		res += "\n\t\t\t}"
		return res

	def initPageSpecific(self):
		self.gbs = wx.GridBagSizer(12, 2)

		self.pageAddStatictext("Name:", (0,0))
		self.pageAddTextctrl("tc_name", self.name, (1,0), (1,2))
		self.pageAddButton("bt_edit", "Edit Mesh", (2,0))
		self.pageAddButton("bt_import", "Import", (2,1))

		self.pageAddStatictext("Mesh Index:", (3,0))
		self.pageAddSpinctrl("sc_mesh_index", 0, None, self.properties["mesh_index"], (3,1))

		self.pageAddStatictext("Path: {}".format(self.properties["path"]), (4,0), name="st_path")

		self.pageAddButton("bt_ok", "OK", (5,0))

		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.page.SetSizer(self.sizer)

	def onTextSpecific(self, event):
		return True
	def onButtonSpecific(self, event):
		bt = event.GetEventObject()
		if bt == self.inputs["bt_ok"]:
			self.destroyPage()
		elif bt == self.inputs["bt_edit"]:
			return self.top.editResource(self)
		elif bt == self.inputs["bt_import"]:
			wildcards = (
				"Wavefront Meshes (*.obj,*.mtl)|*.obj;*.mtl|"
				"All files (*)|*"
			)

			d = self.top.rootDir+os.path.dirname(self.properties["path"])
			f = os.path.basename(self.properties["path"])
			if not self.properties["path"]:
				d = os.getcwd()
				f = ""

			dialog = wx.FileDialog(
				self.top, message="Import Mesh",
				defaultDir=d,
				defaultFile=f,
				wildcard=wildcards,
				style=wx.FD_OPEN
			)

			if dialog.ShowModal() == wx.ID_OK:
				path = dialog.GetPath()
				ext = os.path.splitext(path)[1]

				if self.properties["path"] == self.name+ext:
					dialog.Destroy()
					return False

				self.properties["path"] = self.name+ext
				p = self.top.rootDir+self.path+self.properties["path"]
				if path != p:
					shutil.copyfile(path, p)

				mtl = os.path.splitext(path)[0]+".mtl"
				self.properties["has_material"] = os.path.isfile(mtl)
				if self.properties["has_material"]:
					mp = self.top.rootDir + self.path+self.name+".mtl"
					if mtl != mp:
						shutil.copyfile(mtl, mp)

				self.update()
			else:
				dialog.Destroy()
				return False

			dialog.Destroy()

		return True

	def update(self, res=None):
		p = {k : v for k, v in self.properties.items()}
		if res:
			BEEFBaseResource.update(self, res)
			self.inputs["tc_name"].SetValue(self.name)

			p["mesh_index"] = int(res.inputs["sc_mesh_index"].GetValue())

		self.inputs["sc_mesh_index"].SetValue(p["mesh_index"])
		self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))

	def commitPage(self):
		if self.page:
			tc_name = self.inputs["tc_name"]
			if tc_name.GetValue() != self.name:
				self.rename(tc_name.GetValue())

			self.properties["mesh_index"] = int(self.inputs["sc_mesh_index"].GetValue())
	def moveTo(self, name, newfile):
		if self.properties["path"]:
			ext = os.path.splitext(self.properties["path"])[1]
			os.rename(self.top.rootDir+self.path+self.properties["path"], newfile+ext)
			self.properties["path"] = name+ext
			self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))

	def MenuDuplicate(self, event):
		r = BEEFMesh(self.top, None)
		r.properties = copy.deepcopy(self.properties)
		self.top.addMesh(self.name, r)
