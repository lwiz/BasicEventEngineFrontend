# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import copy
import os
import shutil
import json

from resource.base import BEEFBaseResource
from resource.enum import EResource

class BEEFRoom(BEEFBaseResource):
	defaultBackground = {
		"texture": "",
		"is_visible": True,
		"is_foreground": False,
		"transform": {
			"x": 0,
			"y": 0,
			"is_horizontal_tile": False,
			"is_vertical_tile": False,
			"horizontal_speed": 0,
			"vertical_speed": 0,
			"is_stretched": False
		}
	}
	defaultViewPort = {
		"is_active": True,
		"view": [0, 0, -1, -1],
		"port": [0, 0, -1, -1]
	}

	def __init__(self, top, name):
		BEEFBaseResource.__init__(self, top, name)
		self.path = "/resources/rooms/"
		self.type = EResource.ROOM
		self.properties = {
			"is_persistent": False,
			"width": 1920,
			"height": 1080,

			# See the above defaults for structure details
			"backgrounds": {},
			"viewports": {},

			"instances": [], # Each Instance is as follows: ([object components, ...], (x, y, z), {properties, ...})
			"gravity": (0.0, 0.0, 0.0),
			"scale": 10.0
		}
		self.tmpBackgrounds = {}
		self.tmpViewPorts = {}

	def getBackgroundInit(self):
		bks = self.properties["backgrounds"]
		if self.tmpBackgrounds:
			bks = self.tmpBackgrounds
		return "\n".join(["self.add_background(\"{}\", {})".format(bk_name, bk_data) for bk_name, bk_data in bks.items()])
	def getViewPortInit(self):
		vps = self.properties["viewports"]
		if self.tmpViewPorts:
			vps = self.tmpViewPorts
		return "\n".join(["self.add_viewport(\"{}\", {})".format(vp_name, vp_data) for vp_name, vp_data in vps.items()])
	def getInitCode(self):
		return "pass"
	def getStartCode(self):
		return "pass"
	def getEndCode(self):
		return "pass"
	def getInstanceMap(self):
		return ["[[{}], {}, {}]".format(", ".join(["\"{}\"".format(oc) for oc in inst[0]]), list(inst[1]), json.dumps(inst[2])) for inst in self.properties["instances"]]
	def getRes(self):
		res = "\t\t\t{\n"
		props = [
			"\"name\": \"{}\"".format(self.name),
			"\"path\": \"$/{}.py\"".format(self.name),
			"\"is_persistent\": {}".format("true" if self.properties["is_persistent"] else "false"),
			"\"width\": {}".format(self.properties["width"]),
			"\"height\": {}".format(self.properties["height"]),
			"\"backgrounds\": {}".format(json.dumps(self.properties["backgrounds"])),
			"\"viewports\": {}".format(json.dumps(self.properties["viewports"])),
			"\"instancemap\": \"$/{}.insts.json\"".format(self.name),
			"\"gravity\": [{}, {}, {}]".format(*self.properties["gravity"]),
			"\"scale\": {}".format(self.properties["scale"])
		]
		res += "\t\t\t\t" + ",\n\t\t\t\t".join(props)
		res += "\n\t\t\t}"
		return res

	def chooseBackground(self, bk_name):
		if bk_name in self.tmpBackgrounds:
			self.inputs["bt_background_rename"].Enable()
			self.inputs["cb_bk_visible"].Enable()
			self.inputs["cb_bk_foreground"].Enable()
			self.inputs["ch_bk_texture"].Enable()
			self.inputs["tc_bk_x"].Enable()
			self.inputs["tc_bk_y"].Enable()
			self.inputs["cb_bk_htile"].Enable()
			self.inputs["cb_bk_vtile"].Enable()
			self.inputs["tc_bk_hspeed"].Enable()
			self.inputs["tc_bk_vspeed"].Enable()
			self.inputs["cb_bk_stretch"].Enable()

			ch = self.inputs["ch_background"]
			ch.SetSelection(ch.FindString(bk_name))

			self.inputs["cb_bk_visible"].SetValue(self.tmpBackgrounds[bk_name]["is_visible"])
			self.inputs["cb_bk_foreground"].SetValue(self.tmpBackgrounds[bk_name]["is_foreground"])
			self.inputs["ch_bk_texture"].SetSelection(self.inputs["ch_bk_texture"].FindString(self.tmpBackgrounds[bk_name]["texture"]))

			self.inputs["tc_bk_x"].SetValue(str(self.tmpBackgrounds[bk_name]["transform"]["x"]))
			self.inputs["tc_bk_y"].SetValue(str(self.tmpBackgrounds[bk_name]["transform"]["y"]))
			self.inputs["cb_bk_htile"].SetValue(self.tmpBackgrounds[bk_name]["transform"]["is_horizontal_tile"])
			self.inputs["cb_bk_vtile"].SetValue(self.tmpBackgrounds[bk_name]["transform"]["is_vertical_tile"])
			self.inputs["tc_bk_hspeed"].SetValue(str(self.tmpBackgrounds[bk_name]["transform"]["horizontal_speed"]))
			self.inputs["tc_bk_vspeed"].SetValue(str(self.tmpBackgrounds[bk_name]["transform"]["vertical_speed"]))
			self.inputs["cb_bk_stretch"].SetValue(self.tmpBackgrounds[bk_name]["transform"]["is_stretched"])
		else:
			self.inputs["bt_background_rename"].Enable(False)
			self.inputs["cb_bk_visible"].Enable(False)
			self.inputs["cb_bk_foreground"].Enable(False)
			self.inputs["ch_bk_texture"].Enable(False)
			self.inputs["tc_bk_x"].Enable(False)
			self.inputs["tc_bk_y"].Enable(False)
			self.inputs["cb_bk_htile"].Enable(False)
			self.inputs["cb_bk_vtile"].Enable(False)
			self.inputs["tc_bk_hspeed"].Enable(False)
			self.inputs["tc_bk_vspeed"].Enable(False)
			self.inputs["cb_bk_stretch"].Enable(False)
	def chooseViewPort(self, vp_name):
		if vp_name in self.tmpViewPorts:
			self.inputs["bt_viewport_rename"].Enable()
			self.inputs["cb_vp_active"].Enable()
			self.inputs["tc_vp_room_x"].Enable()
			self.inputs["tc_vp_room_y"].Enable()
			self.inputs["tc_vp_room_w"].Enable()
			self.inputs["tc_vp_room_h"].Enable()
			self.inputs["tc_vp_screen_x"].Enable()
			self.inputs["tc_vp_screen_y"].Enable()
			self.inputs["tc_vp_screen_w"].Enable()
			self.inputs["tc_vp_screen_h"].Enable()

			ch = self.inputs["ch_viewport"]
			ch.SetSelection(ch.FindString(vp_name))

			self.inputs["cb_vp_active"].SetValue(self.tmpViewPorts[vp_name]["is_active"])
			self.inputs["tc_vp_room_x"].SetValue(str(self.tmpViewPorts[vp_name]["view"][0]))
			self.inputs["tc_vp_room_y"].SetValue(str(self.tmpViewPorts[vp_name]["view"][1]))
			self.inputs["tc_vp_room_w"].SetValue(str(self.tmpViewPorts[vp_name]["view"][2]))
			self.inputs["tc_vp_room_h"].SetValue(str(self.tmpViewPorts[vp_name]["view"][3]))
			self.inputs["tc_vp_screen_x"].SetValue(str(self.tmpViewPorts[vp_name]["port"][0]))
			self.inputs["tc_vp_screen_y"].SetValue(str(self.tmpViewPorts[vp_name]["port"][1]))
			self.inputs["tc_vp_screen_w"].SetValue(str(self.tmpViewPorts[vp_name]["port"][2]))
			self.inputs["tc_vp_screen_h"].SetValue(str(self.tmpViewPorts[vp_name]["port"][3]))
		else:
			self.inputs["bt_viewport_rename"].Enable(False)
			self.inputs["cb_vp_active"].Enable(False)
			self.inputs["tc_vp_room_x"].Enable(False)
			self.inputs["tc_vp_room_y"].Enable(False)
			self.inputs["tc_vp_room_w"].Enable(False)
			self.inputs["tc_vp_room_h"].Enable(False)
			self.inputs["tc_vp_screen_x"].Enable(False)
			self.inputs["tc_vp_screen_y"].Enable(False)
			self.inputs["tc_vp_screen_w"].Enable(False)
			self.inputs["tc_vp_screen_h"].Enable(False)
	def initPageSpecific(self):
		self.gbs = wx.GridBagSizer(8, 4)

		# Column 1
		self.pageAddStatictext("Name:", (0,0))
		self.pageAddTextctrl("tc_name", self.name, (1,0), (1,2))

		# Settings
		settingsSizer = wx.StaticBoxSizer(wx.VERTICAL, self.page, "Settings")
		cb_is_persistent = self.pageMakeCheckbox("cb_is_persistent", "Persistent Instances", self.properties["is_persistent"])
		st_width = self.pageMakeStatictext("Width:")
		tc_width = self.pageMakeTextctrl("tc_width", str(self.properties["width"]))
		st_height = self.pageMakeStatictext("Height:")
		tc_height = self.pageMakeTextctrl("tc_height", str(self.properties["height"]))
		settingsSizer.AddMany([(cb_is_persistent), (st_width), (tc_width), (st_height), (tc_height)])
		self.gbs.Add(settingsSizer, (2,0), (1,1))

		# Instances
		instanceSizer = wx.StaticBoxSizer(wx.VERTICAL, self.page, "Instances")
		st_instance = self.pageMakeStatictext("Creating instances for:")
		ch_instance = self.pageMakeChoice("ch_instance", [obj.name for obj in self.top.objects], 0)
		instanceSizer.AddMany([(st_instance), (ch_instance)])
		self.gbs.Add(instanceSizer, (2,1), (1,1))

		# Backgrounds
		self.tmpBackgrounds = copy.deepcopy(self.properties["backgrounds"])
		backgroundSizer = wx.StaticBoxSizer(wx.VERTICAL, self.page, "Backgrounds")

		backgroundChoiceSizer = wx.BoxSizer(wx.HORIZONTAL)
		ch_background = self.pageMakeChoice("ch_background", [bk_name for bk_name in self.tmpBackgrounds], 0)
		bt_background_add = self.pageMakeButton("bt_background_add", "+", wx.BU_EXACTFIT)
		bt_background_remove = self.pageMakeButton("bt_background_remove", "-", wx.BU_EXACTFIT)
		bt_background_rename = self.pageMakeButton("bt_background_rename", "*", wx.BU_EXACTFIT)
		backgroundChoiceSizer.AddMany([(ch_background), (bt_background_add), (bt_background_remove), (bt_background_rename)])

		cb_bk_visible = self.pageMakeCheckbox("cb_bk_visible", "Initially Visible", None)
		cb_bk_foreground = self.pageMakeCheckbox("cb_bk_foreground", "Foreground", None)
		st_bk_texture = self.pageMakeStatictext("Texture:")
		ch_bk_texture = self.pageMakeChoice("ch_bk_texture", [tex.name for tex in self.top.textures], 0)
		st_bk_coords = self.pageMakeStatictext("Coords (X, Y):")
		tc_bk_x = self.pageMakeTextctrl("tc_bk_x", "0")
		tc_bk_y = self.pageMakeTextctrl("tc_bk_y", "0")
		cb_bk_htile = self.pageMakeCheckbox("cb_bk_htile", "Tile Horizontally", None)
		cb_bk_vtile = self.pageMakeCheckbox("cb_bk_vtile", "Tile Vertically", None)
		st_bk_speed = self.pageMakeStatictext("Speed (X, Y):")
		tc_bk_hspeed = self.pageMakeTextctrl("tc_bk_hspeed", "0")
		tc_bk_vspeed = self.pageMakeTextctrl("tc_bk_vspeed", "0")
		cb_bk_stretch = self.pageMakeCheckbox("cb_bk_stretch", "Stretch to Screen", None)

		if self.properties["backgrounds"]:
			self.chooseBackground(ch_background.GetString(0))
		else:
			self.chooseBackground(None)
		backgroundSizer.AddMany([
			(backgroundChoiceSizer), (cb_bk_visible), (cb_bk_foreground), (st_bk_texture), (ch_bk_texture),
			(st_bk_coords), (tc_bk_x), (tc_bk_y), (cb_bk_htile), (cb_bk_vtile), (st_bk_speed), (tc_bk_hspeed), (tc_bk_vspeed), (cb_bk_stretch)
		])
		self.gbs.Add(backgroundSizer, (3,0), (1,1))

		# Physics
		physicsSizer = wx.StaticBoxSizer(wx.VERTICAL, self.page, "Physics")
		st_gravity = self.pageMakeStatictext("Gravity (X, Y, Z):")
		sc_gravity_x = self.pageMakeSpinctrl("sc_gravity_x", None, None, self.properties["gravity"][0], isFloat=True)
		sc_gravity_y = self.pageMakeSpinctrl("sc_gravity_y", None, None, self.properties["gravity"][1], isFloat=True)
		sc_gravity_z = self.pageMakeSpinctrl("sc_gravity_z", None, None, self.properties["gravity"][2], isFloat=True)
		st_scale = self.pageMakeStatictext("Scale:")
		sc_scale = self.pageMakeSpinctrl("sc_scale", 0.0, None, self.properties["scale"], isFloat=True)
		physicsSizer.AddMany([(st_gravity), (sc_gravity_x), (sc_gravity_y), (sc_gravity_z), (st_scale), (sc_scale)])
		self.gbs.Add(physicsSizer, (3,1), (1,1))

		# ViewPorts
		self.tmpViewPorts = copy.deepcopy(self.properties["viewports"])
		viewportSizer = wx.StaticBoxSizer(wx.VERTICAL, self.page, "View Ports")

		viewportChoiceSizer = wx.BoxSizer(wx.HORIZONTAL)
		ch_viewport = self.pageMakeChoice("ch_viewport", [vp_name for vp_name in self.tmpViewPorts], 0)
		bt_viewport_add = self.pageMakeButton("bt_viewport_add", "+", wx.BU_EXACTFIT)
		bt_viewport_remove = self.pageMakeButton("bt_viewport_remove", "-", wx.BU_EXACTFIT)
		bt_viewport_rename = self.pageMakeButton("bt_viewport_rename", "*", wx.BU_EXACTFIT)
		viewportChoiceSizer.AddMany([(ch_viewport), (bt_viewport_add), (bt_viewport_remove), (bt_viewport_rename)])

		cb_vp_active = self.pageMakeCheckbox("cb_vp_active", "Initially Active", None)

		st_vp_room = self.pageMakeStatictext("Room Spot (X, Y, W, H):")
		vp_roomSizer = wx.BoxSizer(wx.HORIZONTAL)
		tc_vp_room_x = self.pageMakeTextctrl("tc_vp_room_x", "0")
		tc_vp_room_y = self.pageMakeTextctrl("tc_vp_room_y", "0")
		tc_vp_room_w = self.pageMakeTextctrl("tc_vp_room_w", "0")
		tc_vp_room_h = self.pageMakeTextctrl("tc_vp_room_h", "0")
		vp_roomSizer.AddMany([(tc_vp_room_x), (tc_vp_room_y), (tc_vp_room_w), (tc_vp_room_h)])

		st_vp_screen = self.pageMakeStatictext("Screen Spot (X, Y, W, H):")
		vp_screenSizer = wx.BoxSizer(wx.HORIZONTAL)
		tc_vp_screen_x = self.pageMakeTextctrl("tc_vp_screen_x", "0")
		tc_vp_screen_y = self.pageMakeTextctrl("tc_vp_screen_y", "0")
		tc_vp_screen_w = self.pageMakeTextctrl("tc_vp_screen_w", "0")
		tc_vp_screen_h = self.pageMakeTextctrl("tc_vp_screen_h", "0")
		vp_screenSizer.AddMany([(tc_vp_screen_x), (tc_vp_screen_y), (tc_vp_screen_w), (tc_vp_screen_h)])

		if self.properties["viewports"]:
			self.chooseViewPort(ch_viewport.GetString(0))
		else:
			self.chooseViewPort(None)
		viewportSizer.AddMany([
			(viewportChoiceSizer), (cb_vp_active), (st_vp_room), (vp_roomSizer), (st_vp_screen), (vp_screenSizer)
		])
		self.gbs.Add(viewportSizer, (4,0), (1,2))

		self.pageAddButton("bt_ok", "OK", (6,0))

		# Column 2
		gr = self.pageAddGrid("gr_instances", (0,2), (7,1))
		for inst in self.properties["instances"]:
			gr.AddInstance(inst)
		if self.top.objects:
			gr.SetObject(self.top.objects[0].name)
		gr.SetDimensions(self.properties["width"], self.properties["height"])
		gr.Redraw()

		self.gbs.AddGrowableRow(5)
		self.gbs.AddGrowableCol(2)
		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.page.SetSizer(self.sizer)

	def onTextSpecific(self, event):
		tc = event.GetEventObject()
		if tc == self.inputs["tc_width"] or tc == self.inputs["tc_height"]:
			gr = self.inputs["gr_instances"]
			gr.SetDimensions(int(self.inputs["tc_width"].GetValue()), int(self.inputs["tc_height"].GetValue()))
			gr.Redraw()
		elif tc == self.inputs["tc_bk_x"]:
			ch = self.inputs["ch_background"]
			try:
				self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["transform"]["x"] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_bk_y"]:
			ch = self.inputs["ch_background"]
			try:
				self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["transform"]["y"] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_bk_hspeed"]:
			ch = self.inputs["ch_background"]
			try:
				self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["transform"]["horizontal_speed"] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_bk_vspeed"]:
			ch = self.inputs["ch_background"]
			try:
				self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["transform"]["vertical_speed"] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_vp_room_x"]:
			ch = self.inputs["ch_viewport"]
			try:
				self.tmpViewPorts[ch.GetString(ch.GetSelection())]["view"][0] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_vp_room_y"]:
			ch = self.inputs["ch_viewport"]
			try:
				self.tmpViewPorts[ch.GetString(ch.GetSelection())]["view"][1] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_vp_room_w"]:
			ch = self.inputs["ch_viewport"]
			try:
				self.tmpViewPorts[ch.GetString(ch.GetSelection())]["view"][2] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_vp_room_h"]:
			ch = self.inputs["ch_viewport"]
			try:
				self.tmpViewPorts[ch.GetString(ch.GetSelection())]["view"][3] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_vp_screen_x"]:
			ch = self.inputs["ch_viewport"]
			try:
				self.tmpViewPorts[ch.GetString(ch.GetSelection())]["view"][0] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_vp_screen_y"]:
			ch = self.inputs["ch_viewport"]
			try:
				self.tmpViewPorts[ch.GetString(ch.GetSelection())]["view"][1] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_vp_screen_w"]:
			ch = self.inputs["ch_viewport"]
			try:
				self.tmpViewPorts[ch.GetString(ch.GetSelection())]["view"][2] = int(tc.GetValue())
			except ValueError:
				pass
		elif tc == self.inputs["tc_vp_screen_h"]:
			ch = self.inputs["ch_viewport"]
			try:
				self.tmpViewPorts[ch.GetString(ch.GetSelection())]["view"][3] = int(tc.GetValue())
			except ValueError:
				pass

		return True
	def onCheckBoxSpecific(self, event):
		cb = event.GetEventObject()
		if cb == self.inputs["cb_bk_visible"]:
			ch = self.inputs["ch_background"]
			self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["is_visible"] = cb.GetValue()
		elif cb == self.inputs["cb_bk_foreground"]:
			ch = self.inputs["ch_background"]
			self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["is_foreground"] = cb.GetValue()
		elif cb == self.inputs["cb_bk_htile"]:
			ch = self.inputs["ch_background"]
			self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["transform"]["is_horizontal_tile"] = cb.GetValue()
		elif cb == self.inputs["cb_bk_vtile"]:
			ch = self.inputs["ch_background"]
			self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["transform"]["is_vertical_tile"] = cb.GetValue()
		elif cb == self.inputs["cb_bk_stretch"]:
			ch = self.inputs["ch_background"]
			self.tmpBackgrounds[ch.GetString(ch.GetSelection())]["transform"]["is_stretched"] = cb.GetValue()
		elif cb == self.inputs["cb_vp_active"]:
			ch = self.inputs["ch_viewport"]
			self.tmpViewPorts[ch.GetString(ch.GetSelection())]["is_active"] = cb.GetValue()

		return True
	def onButtonSpecific(self, event):
		bt = event.GetEventObject()
		if bt == self.inputs["bt_ok"]:
			self.destroyPage()
		elif bt == self.inputs["bt_background_add"]:
			name = self.top.dialogRename("bk_"+str(len(self.tmpBackgrounds)), is_conflict=False, title="Background Naming", msg="Choose a name for the new background:")
			if name:
				while name in self.tmpBackgrounds:
					name = self.top.dialogRename(name)

				self.tmpBackgrounds[name] = copy.deepcopy(BEEFRoom.defaultBackground)
				self.inputs["ch_background"].Append(name)

				self.chooseBackground(name)
		elif bt == self.inputs["bt_background_remove"]:
			ch = self.inputs["ch_background"]
			cur_bk = ch.GetString(ch.GetSelection())
			if cur_bk in self.tmpBackgrounds:
				del self.tmpBackgrounds[cur_bk]
				ch.Delete(ch.GetSelection())

				self.chooseBackground(ch.GetString(ch.GetCount()-1))
		elif bt == self.inputs["bt_background_rename"]:
			ch = self.inputs["ch_background"]
			old_name = ch.GetString(ch.GetSelection())
			name = self.top.dialogRename(old_name, is_conflict=False, title="Background Renaming", msg="Choose a new name for the background:")
			if name:
				while name in self.tmpBackgrounds and name != old_name:
					name = self.top.dialogRename(name)

				self.tmpBackgrounds[name] = self.tmpBackgrounds.pop(old_name)
				ch.SetString(ch.GetSelection(), name)
		elif bt == self.inputs["bt_viewport_add"]:
			name = self.top.dialogRename("vp_"+str(len(self.tmpViewPorts)), is_conflict=False, title="View Port Naming", msg="Choose a name for the new viewport:")
			if name:
				while name in self.tmpViewPorts:
					name = self.top.dialogRename(name)

				self.tmpViewPorts[name] = copy.deepcopy(BEEFRoom.defaultViewPort)
				self.inputs["ch_viewport"].Append(name)

				self.chooseViewPort(name)
		elif bt == self.inputs["bt_viewport_remove"]:
			ch = self.inputs["ch_viewport"]
			cur_vp = ch.GetString(ch.GetSelection())
			if cur_vp in self.tmpViewPorts:
				del self.tmpViewPorts[cur_vp]
				ch.Delete(ch.GetSelection())

				self.chooseViewPort(ch.GetString(ch.GetCount()-1))
		elif bt == self.inputs["bt_viewport_rename"]:
			ch = self.inputs["ch_viewport"]
			old_name = ch.GetString(ch.GetSelection())
			name = self.top.dialogRename(old_name, is_conflict=False, title="View Port Renaming", msg="Choose a new name for the viewport:")
			if name:
				while name in self.tmpViewPorts and name != old_name:
					name = self.top.dialogRename(name)

				self.tmpViewPorts[name] = self.tmpViewPorts.pop(old_name)
				ch.SetString(ch.GetSelection(), name)

		return True
	def onSpinCtrlSpecific(self, event):
		return True
	def onChoiceSpecific(self, event):
		ch = event.GetEventObject()
		if ch == self.inputs["ch_instance"]:
			self.inputs["gr_instances"].SetObject(ch.GetString(ch.GetSelection()))
		elif ch == self.inputs["ch_background"]:
			self.chooseBackground(ch.GetString(ch.GetSelection()))
		elif ch == self.inputs["ch_viewport"]:
			self.chooseViewPort(ch.GetString(ch.GetSelection()))
		elif ch == self.inputs["ch_bk_texture"]:
			ch_background = self.inputs["ch_background"]
			self.tmpBackgrounds[ch_background.GetString(ch_background.GetSelection())]["texture"] = ch.GetString(ch.GetSelection())

		return False

	def update(self, res=None):
		p = {k : v for k, v in self.properties.items()}
		cur_bk = self.inputs["ch_background"].GetString(self.inputs["ch_background"].GetSelection())
		cur_vp = self.inputs["ch_viewport"].GetString(self.inputs["ch_viewport"].GetSelection())
		if res:
			BEEFBaseResource.update(self, res)
			self.inputs["tc_name"].SetValue(self.name)

			cur_bk = res.inputs["ch_background"].GetString(res.inputs["ch_background"].GetSelection())
			cur_vp = res.inputs["ch_viewport"].GetString(res.inputs["ch_viewport"].GetSelection())

			p["is_persistent"] = res.inputs["cb_is_persistent"].GetValue()
			p["width"] = res.inputs["tc_width"].GetValue()
			p["height"] = res.inputs["tc_height"].GetValue()
			p["backgrounds"] = res.tmpBackgrounds
			p["viewports"] = res.tmpViewPorts
			p["instances"] = [inst for inst in res.inputs["gr_instances"].instances]
			p["gravity"] = (res.inputs["sc_gravity_x"].GetValue(), res.inputs["sc_gravity_y"].GetValue(), res.inputs["sc_gravity_z"].GetValue())
			p["scale"] = res.inputs["sc_scale"].GetValue()

		self.inputs["cb_is_persistent"].SetValue(p["is_persistent"])
		self.inputs["tc_width"].SetValue(p["width"])
		self.inputs["tc_height"].SetValue(p["height"])

		self.tmpBackgrounds = p["backgrounds"]
		self.chooseBackground(cur_bk)
		self.tmpViewPorts = p["viewports"]
		self.chooseViewPort(cur_vp)

		gr = self.inputs["gr_instances"]
		gr.ClearInstances()
		for inst in p["instances"]:
			gr.AddInstance(inst)
		gr.SetDimensions(p["width"], p["height"])
		gr.Redraw()

		self.inputs["sc_gravity_x"].SetValue(p["gravity"][0])
		self.inputs["sc_gravity_y"].SetValue(p["gravity"][1])
		self.inputs["sc_gravity_z"].SetValue(p["gravity"][2])
		self.inputs["sc_scale"].SetValue(p["scale"])

	def commitPage(self):
		if self.page:
			tc_name = self.inputs["tc_name"]
			if tc_name.GetValue() != self.name:
				self.rename(tc_name.GetValue())

			self.properties["is_persistent"] = self.inputs["cb_is_persistent"].GetValue()
			self.properties["width"] = int(self.inputs["tc_width"].GetValue())
			self.properties["height"] = int(self.inputs["tc_height"].GetValue())

			self.properties["backgrounds"] = self.tmpBackgrounds
			self.properties["viewports"] = self.tmpViewPorts

			self.properties["instances"] = [inst for inst in self.inputs["gr_instances"].instances]
			self.properties["gravity"] = (self.inputs["sc_gravity_x"].GetValue(), self.inputs["sc_gravity_y"].GetValue(), self.inputs["sc_gravity_z"].GetValue())
			self.properties["scale"] = self.inputs["sc_scale"].GetValue()
	def moveTo(self, name, newfile):
		"""if self.properties["path"]:
			ext = os.path.splitext(self.properties["path"])[1]
			os.rename(self.top.rootDir+self.properties["path"], newfile+ext)
			self.properties["path"] = self.path+name+ext
			self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))"""

	def MenuDuplicate(self, event):
		r = BEEFRoom(self.top, None)
		r.properties = copy.deepcopy(self.properties)
		self.top.addRoom(self.name, r)
