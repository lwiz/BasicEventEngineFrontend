# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import copy
import os
import shutil

from resource.base import BEEFBaseResource
from resource.enum import EResource
from resource.enum import ELight

class BEEFLight(BEEFBaseResource):
	def __init__(self, top, name):
		BEEFBaseResource.__init__(self, top, name)
		self.path = "/resources/lights/"
		self.type = EResource.LIGHT
		self.properties = {
			"type": ELight.AMBIENT,
			"position": (0.0, 0.0, 0.0),
			"direction": (0.0, 0.0, 0.0),
			"attenuation": (0.0, 0.0, 0.0, 0.0),
			"color": (255, 255, 255, 255)
		}

	def getRes(self):
		res = "\t\t\t{\n"
		props = [
			"\"name\": \"{}\"".format(self.name),
			"\"path\": \"$/{}.data.json\"".format(self.name),
		]
		res += "\t\t\t\t" + ",\n\t\t\t\t".join(props)
		res += "\n\t\t\t}"
		return res

	def activateType(self, type):
		enable = []
		disable = []

		if type == ELight.AMBIENT:
			enable = ["sc_color_r", "sc_color_g", "sc_color_b", "sc_color_a"]
			disable = ["sc_position_x", "sc_position_y", "sc_position_z", "sc_direction_x", "sc_direction_y", "sc_direction_z", "sc_attenuation_x", "sc_attenuation_y", "sc_attenuation_z", "sc_attenuation_w"]
		elif type == ELight.DIFFUSE:
			enable = ["sc_direction_x", "sc_direction_y", "sc_direction_z", "sc_color_r", "sc_color_g", "sc_color_b", "sc_color_a"]
			disable = ["sc_position_x", "sc_position_y", "sc_position_z", "sc_attenuation_x", "sc_attenuation_y", "sc_attenuation_z", "sc_attenuation_w"]
		elif type == ELight.POINT:
			enable = ["sc_position_x", "sc_position_y", "sc_position_z", "sc_attenuation_x", "sc_attenuation_y", "sc_attenuation_z", "sc_attenuation_w", "sc_color_r", "sc_color_g", "sc_color_b", "sc_color_a"]
			disable = ["sc_direction_x", "sc_direction_y", "sc_direction_z"]
		elif type == ELight.SPOT:
			enable = ["sc_position_x", "sc_position_y", "sc_position_z", "sc_direction_x", "sc_direction_y", "sc_direction_z", "sc_attenuation_x", "sc_attenuation_y", "sc_attenuation_z", "sc_attenuation_w", "sc_color_r", "sc_color_g", "sc_color_b", "sc_color_a"]
			disable = []

		for e in enable:
			self.inputs[e].Enable(True)
		for e in disable:
			self.inputs[e].Enable(False)
	def getCurrentColor(self):
		sc_color_r = self.inputs["sc_color_r"]
		sc_color_g = self.inputs["sc_color_g"]
		sc_color_b = self.inputs["sc_color_b"]
		sc_color_a = self.inputs["sc_color_a"]
		return (sc_color_r.GetValue(), sc_color_g.GetValue(), sc_color_b.GetValue(), sc_color_a.GetValue())
	def updateColorButton(self):
		bt_color = self.inputs["bt_color"]
		bt_color.SetBackgroundColour(self.getCurrentColor())

	def initPageSpecific(self):
		self.gbs = wx.GridBagSizer(12, 2)

		self.pageAddStatictext("Name:", (0,0))
		self.pageAddTextctrl("tc_name", self.name, (1,0), (1,2))

		self.pageAddStatictext("Type:", (2,0))
		self.pageAddChoice("ch_type", ["Ambient", "Diffuse", "Point", "Spot"], self.properties["type"]-1, (2,1))

		self.pageAddStatictext("Position:", (3,0))
		self.pageAddSpinctrl("sc_position_x", None, None, self.properties["position"][0], (3,1), isFloat=True)
		self.pageAddSpinctrl("sc_position_y", None, None, self.properties["position"][1], (3,2), isFloat=True)
		self.pageAddSpinctrl("sc_position_z", None, None, self.properties["position"][2], (3,3), isFloat=True)

		self.pageAddStatictext("Direction:", (4,0))
		self.pageAddSpinctrl("sc_direction_x", None, None, self.properties["direction"][0], (4,1), isFloat=True)
		self.pageAddSpinctrl("sc_direction_y", None, None, self.properties["direction"][1], (4,2), isFloat=True)
		self.pageAddSpinctrl("sc_direction_z", None, None, self.properties["direction"][2], (4,3), isFloat=True)

		self.pageAddStatictext("Attenuation:", (5,0))
		self.pageAddSpinctrl("sc_attenuation_x", None, None, self.properties["attenuation"][0], (5,1), isFloat=True)
		self.pageAddSpinctrl("sc_attenuation_y", None, None, self.properties["attenuation"][1], (5,2), isFloat=True)
		self.pageAddSpinctrl("sc_attenuation_z", None, None, self.properties["attenuation"][2], (5,3), isFloat=True)
		self.pageAddSpinctrl("sc_attenuation_w", None, None, self.properties["attenuation"][3], (5,4), isFloat=True)

		self.pageAddStatictext("Color (RGBA):", (6,0))
		self.pageAddSpinctrl("sc_color_r", 0, 255, self.properties["color"][0], (6,1))
		self.pageAddSpinctrl("sc_color_g", 0, 255, self.properties["color"][1], (6,2))
		self.pageAddSpinctrl("sc_color_b", 0, 255, self.properties["color"][2], (6,3))
		self.pageAddSpinctrl("sc_color_a", 0, 255, self.properties["color"][3], (6,4))
		bt_color = self.pageAddButton("bt_color", "Color Picker", (6,5))
		self.updateColorButton()

		self.pageAddButton("bt_ok", "OK", (7,0))

		self.sizer = wx.BoxSizer()
		self.sizer.Add(self.gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.page.SetSizer(self.sizer)

		self.activateType(self.properties["type"])

	def onTextSpecific(self, event):
		return True
	def onButtonSpecific(self, event):
		bt = event.GetEventObject()
		if bt == self.inputs["bt_ok"]:
			self.destroyPage()
		elif bt == self.inputs["bt_color"]:
			dialog = wx.ColourDialog(self.page)
			dialog.GetColourData().SetColour(wx.Colour(self.getCurrentColor()))

			if dialog.ShowModal() == wx.ID_OK:
				color = dialog.GetColourData().GetColour().Get(True)
				self.inputs["sc_color_r"].SetValue(color[0])
				self.inputs["sc_color_g"].SetValue(color[1])
				self.inputs["sc_color_b"].SetValue(color[2])
				#self.inputs["sc_color_a"].SetValue(color[3])

				self.updateColorButton()
			else:
				dialog.Destroy()
				return False

			dialog.Destroy()

		return True
	def onSpinCtrlSpecific(self, event):
		sc = event.GetEventObject()
		if sc in [self.inputs["sc_color_"+c] for c in ["r", "g", "b", "a"]]: # For sc_color_r, _g, _b, and _a
			self.updateColorButton()

		return True
	def onChoiceSpecific(self, event):
		ch = event.GetEventObject()
		if ch == self.inputs["ch_type"]:
			self.properties["type"] = ch.GetSelection()+1
			self.activateType(self.properties["type"])

		return True

	def update(self, res=None):
		p = {k : v for k, v in self.properties.items()}
		if res:
			BEEFBaseResource.update(self, res)
			self.inputs["tc_name"].SetValue(self.name)

			p["type"] = res.input["ch_type"].GetSelection()+1

			p["position"] = (res.inputs["sc_position_x"].GetValue(), res.inputs["sc_position_y"].GetValue(), res.inputs["sc_position_z"].GetValue())
			p["direction"] = (res.inputs["sc_direction_x"].GetValue(), res.inputs["sc_direction_y"].GetValue(), res.inputs["sc_direction_z"].GetValue())
			p["attenuation"] = (res.inputs["sc_attenuation_x"].GetValue(), res.inputs["sc_attenuation_y"].GetValue(), res.inputs["sc_attenuation_z"].GetValue(), res.inputs["sc_attenuation_w"].GetValue())
			p["color"] = (res.inputs["sc_color_r"].GetValue(), res.inputs["sc_color_g"].GetValue(), res.inputs["sc_color_b"].GetValue(), res.inputs["sc_color_a"].GetValue())

		self.inputs["ch_type"].SetSelection(p["type"]-1)

		self.inputs["sc_position_x"].SetValue(p["position"][0])
		self.inputs["sc_position_y"].SetValue(p["position"][1])
		self.inputs["sc_position_z"].SetValue(p["position"][2])

		self.inputs["sc_direction_x"].SetValue(p["direction"][0])
		self.inputs["sc_direction_y"].SetValue(p["direction"][1])
		self.inputs["sc_direction_z"].SetValue(p["direction"][2])

		self.inputs["sc_attenuation_x"].SetValue(p["attenuation"][0])
		self.inputs["sc_attenuation_y"].SetValue(p["attenuation"][1])
		self.inputs["sc_attenuation_z"].SetValue(p["attenuation"][2])
		self.inputs["sc_attenuation_w"].SetValue(p["attenuation"][3])

		self.inputs["sc_color_r"].SetValue(p["color"][0])
		self.inputs["sc_color_g"].SetValue(p["color"][1])
		self.inputs["sc_color_b"].SetValue(p["color"][2])
		self.inputs["sc_color_a"].SetValue(p["color"][3])

	def commitPage(self):
		if self.page:
			tc_name = self.inputs["tc_name"]
			if tc_name.GetValue() != self.name:
				self.rename(tc_name.GetValue())

			ch_type = self.inputs["ch_type"]
			self.properties["type"] = ch_type.GetSelection()+1

			sc_position_x = self.inputs["sc_position_x"]
			sc_position_y = self.inputs["sc_position_y"]
			sc_position_z = self.inputs["sc_position_z"]
			self.properties["position"] = (sc_position_x.GetValue(), sc_position_y.GetValue(), sc_position_z.GetValue())

			sc_direction_x = self.inputs["sc_direction_x"]
			sc_direction_y = self.inputs["sc_direction_y"]
			sc_direction_z = self.inputs["sc_direction_z"]
			self.properties["direction"] = (sc_direction_x.GetValue(), sc_direction_y.GetValue(), sc_direction_z.GetValue())

			sc_attenuation_x = self.inputs["sc_attenuation_x"]
			sc_attenuation_y = self.inputs["sc_attenuation_y"]
			sc_attenuation_z = self.inputs["sc_attenuation_z"]
			sc_attenuation_w = self.inputs["sc_attenuation_w"]
			self.properties["attenuation"] = (sc_attenuation_x.GetValue(), sc_attenuation_y.GetValue(), sc_attenuation_z.GetValue(), sc_attenuation_w.GetValue())

			sc_color_r = self.inputs["sc_color_r"]
			sc_color_g = self.inputs["sc_color_g"]
			sc_color_b = self.inputs["sc_color_b"]
			sc_color_a = self.inputs["sc_color_a"]
			self.properties["color"] = (sc_color_r.GetValue(), sc_color_g.GetValue(), sc_color_b.GetValue(), sc_color_a.GetValue())
	def moveTo(self, name, newfile):
		"""if self.properties["path"]:
			ext = os.path.splitext(self.properties["path"])[1]
			os.rename(self.top.rootDir+self.properties["path"], newfile+ext)
			self.properties["path"] = self.path+name+ext
			self.inputs["st_path"].SetLabel("Path: {}".format(self.properties["path"]))"""

	def MenuDuplicate(self, event):
		r = BEEFLight(self.top, None)
		r.properties = copy.deepcopy(self.properties)
		self.top.addLight(self.name, r)
