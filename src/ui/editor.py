# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import wx.stc

import math
import keyword

class BEEFEditor(wx.stc.StyledTextCtrl):
	def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.LC_ICON):
		wx.stc.StyledTextCtrl.__init__(self, parent, id, pos, size, style)

		self.indent = 4

		self.SetLexer(wx.stc.STC_LEX_PYTHON)

		self.Colourise(0, -1)

		self.SetMarginType(1, wx.stc.STC_MARGIN_NUMBER)
		self.UpdateMargins()

		# Colors based on the Atom One Dark Syntax theme
		mono0 = "#282a2e"
		mono1 = "#9ca4b5"
		mono2 = "#80848c"
		mono3 = "#5c5f66"
		cyan = "#4a858c"
		blue = "#1e6aa8"
		purple = "#9344ab"
		green = "#7b9e62"
		red1 = "#a63a43"
		red2 = "#824944"
		orange1 = "#9c7048"
		orange2 = "#b0873a"

		font = "Consolas"
		size = 12
		fs = "face:{},size:{}".format(font, size)
		bfs = "back:{},{}".format(mono0, fs)
		self.StyleSetSpec(wx.stc.STC_STYLE_DEFAULT,     "fore:{},{}".format(mono1, bfs))

		self.StyleClearAll()
		self.StyleSetSpec(wx.stc.STC_STYLE_DEFAULT,     "fore:{},{}".format(mono1, bfs))
		self.StyleSetSpec(wx.stc.STC_STYLE_LINENUMBER,  "fore:#000000,back:#C0C0C0,{}".format(fs))
		self.StyleSetSpec(wx.stc.STC_STYLE_CONTROLCHAR, "face:{}".format(font))
		self.StyleSetSpec(wx.stc.STC_STYLE_BRACELIGHT,  "fore:#FFFFFF,back:#0000FF,bold")
		self.StyleSetSpec(wx.stc.STC_STYLE_BRACEBAD,    "fore:#000000,back:#FF0000,bold")

		# Python lexer colors
		self.StyleSetSpec(wx.stc.STC_P_DEFAULT, "fore:{},{}".format(mono1, fs))
		self.StyleSetSpec(wx.stc.STC_P_IDENTIFIER, "fore:{},{}".format(blue, fs))
		#self.StyleSetSpec(wx.stc.STC_P_IDENTIFIER, "fore:{},{}".format(mono1, fs))
		self.StyleSetSpec(wx.stc.STC_P_STRING, "fore:{},{}".format(green, fs))
		self.StyleSetSpec(wx.stc.STC_P_NUMBER, "fore:{},{}".format(orange1, fs))
		self.StyleSetSpec(wx.stc.STC_P_CHARACTER, "fore:{},{}".format(green, fs))
		self.StyleSetSpec(wx.stc.STC_P_WORD, "fore:{},{}".format(purple, fs))
		#self.StyleSetSpec(wx.stc.STC_P_WORD2, "fore:{},{}".format(blue, fs))
		self.StyleSetSpec(wx.stc.STC_P_OPERATOR, "fore:{},{}".format(mono1, fs))
		self.StyleSetSpec(wx.stc.STC_P_TRIPLE, "fore:{},{}".format(green, fs))
		self.StyleSetSpec(wx.stc.STC_P_TRIPLEDOUBLE, "fore:{},{}".format(green, fs))

		self.StyleSetSpec(wx.stc.STC_P_CLASSNAME, "fore:{},{}".format(orange2, fs))
		self.StyleSetSpec(wx.stc.STC_P_DEFNAME, "fore:{},{}".format(blue, fs))
		self.StyleSetSpec(wx.stc.STC_P_STRINGEOL, "fore:{},{}".format(green, fs))
		self.StyleSetSpec(wx.stc.STC_P_COMMENTLINE, "fore:{},{},italic".format(mono3, fs))
		self.StyleSetSpec(wx.stc.STC_P_COMMENTBLOCK, "fore:{},{},italic".format(mono3, fs))

		self.SetCaretForeground(wx.Colour(82, 139, 255))
		self.SetCaretWidth(2)
		self.SetCaretLineBackground(wx.Colour(44, 50, 60))
		self.SetCaretLineVisible(True)
		self.SetSelAlpha(20)

		self.SetEdgeMode(wx.stc.STC_EDGE_LINE)
		self.SetEdgeColumn(80)

		self.SetUseTabs(True)
		self.SetTabWidth(self.indent)
		self.SetIndentationGuides(1)

		self.SetKeyWords(0, " ".join(keyword.kwlist))

		self.Bind(wx.stc.EVT_STC_CHARADDED, self.OnCharAdded)
		self.Bind(wx.stc.EVT_STC_CHANGE, self.OnChange)

	def UpdateMargins(self):
		line_amount = self.GetText().count("\n")+1
		self.SetMarginWidth(1, 12.5*(math.floor(math.log(line_amount, 10))+1))
	def SetText(self, text):
		wx.stc.StyledTextCtrl.SetText(self, text)
	def AppendText(self, text):
		wx.stc.StyledTextCtrl.AppendText(self, text)
	def UpdateText(self, text):
		ip = self.GetInsertionPoint()
		line = self.GetFirstVisibleLine()
		self.SetUndoCollection(False)

		self.SetText(text)

		self.SetFirstVisibleLine(line)
		self.SetEmptySelection(ip)
		self.SetUndoCollection(True)
	def OnCharAdded(self, event):
		newline = ord("\r")
		if not self.GetEOLMode() == wx.stc.STC_EOL_CR:
			newline = ord("\n")

		key = event.GetKey()
		if key == newline:
			line = self.GetCurrentLine()
			indent = self.GetLineIndentation(line-1)

			last_line = self.GetLineText(line-1)
			if last_line and last_line[-1] in ":":
				indent += self.indent

			self.SetLineIndentation(line, indent)
			self.GotoPos(self.GetCurrentPos()+indent/4)

		self.UpdateMargins()
		self.Colourise(0, -1)

		event.Skip()
	def OnChange(self, event):
		self.UpdateMargins()
		self.Colourise(0, -1)

		event.Skip()
