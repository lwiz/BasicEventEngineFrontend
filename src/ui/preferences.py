# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import os
import sys

class BEEFPreferences(wx.Dialog):
	def __init__(self, top):
		wx.Dialog.__init__(self)
		self.Create(top, -1, "Edit Preferences", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		self.top = top

		gbs = wx.GridBagSizer(3, 2)

		self.cb_open_console = wx.CheckBox(self, -1, "Open the Console when compiling")
		gbs.Add(self.cb_open_console, (0, 0), (1, 2))

		gbs.Add(wx.StaticText(self, -1, "Default engine:"), (1, 0), (1, 1))
		self.ch_default_engine = wx.Choice(self, -1, choices=[])
		gbs.Add(self.ch_default_engine, (1, 1), (1, 1))

		buttons = wx.StdDialogButtonSizer()
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		buttons.Add(btn)

		btn = wx.Button(self, wx.ID_CANCEL)
		buttons.Add(btn)
		buttons.Realize()
		gbs.Add(buttons, (2, 0), (1, 2))

		sizer = wx.BoxSizer()
		sizer.Add(gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.SetSizer(sizer)
		sizer.Fit(self)

		self.CenterOnScreen()

	def show(self):
		# Update fields
		self.cb_open_console.SetValue(self.top.prefCfg["open_console"])

		chde = self.ch_default_engine
		chde.Clear()
		chde.AppendItems(self.top.engine_manager.getCurrentEngines())
		chde.SetSelection(chde.FindString(self.top.prefCfg["default_engine"]))

		self.Fit()

		# Workaround ignored window positioning
		self.Show(True)
		self.Center()
		self.Show(False)

		val = self.ShowModal()
		if val == wx.ID_OK:
			self.top.prefCfg["open_console"] = self.cb_open_console.GetValue()
			self.top.prefCfg["default_engine"] = self.ch_default_engine.GetString(self.ch_default_engine.GetSelection())

			self.top.savePreferences()
