# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import os
import urllib.request
import json
import hashlib

class BEEFEngineManager(wx.Dialog):
	def __init__(self, top):
		self.url = "https://toni.blue/BasicEventEngine/binaries"

		wx.Dialog.__init__(self)
		self.Create(top, -1, "Manage Engine Versions", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		self.top = top

		gbs = wx.GridBagSizer(4, 2)

		gbs.Add(wx.StaticText(self, -1, "Available Versions:"), (0, 0), (1, 1))
		self.clb_versions = wx.CheckListBox(self, -1)
		gbs.Add(self.clb_versions, (1, 0), (1, 2))

		self.ga_progress = wx.Gauge(self, -1, size=(400, 10))
		self.ga_progress.SetRange(100)
		self.ga_progress.SetValue(0)
		gbs.Add(self.ga_progress, (2, 0), (1, 2))

		buttons = wx.StdDialogButtonSizer()
		btn = wx.Button(self, wx.ID_APPLY)
		self.Bind(wx.EVT_BUTTON, self.Apply, btn)
		buttons.Add(btn)

		btn = wx.Button(self, wx.ID_CLOSE)
		self.Bind(wx.EVT_BUTTON, self.Close, btn)
		btn.SetDefault()
		buttons.Add(btn)
		buttons.Realize()
		gbs.Add(buttons, (3, 0), (1, 2))

		sizer = wx.BoxSizer()
		sizer.Add(gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.SetSizer(sizer)
		sizer.Fit(self)

		self.CenterOnScreen()

	def Apply(self, e):
		def progress(blocks, block_size, total_size):
			if total_size == -1:
				self.ga_progress.Pulse()
			else:
				self.ga_progress.SetRange(total_size)
				try:
					self.ga_progress.SetValue(blocks*block_size)
				except wx._core.wxAssertionError:
					pass

			wx.SafeYield()

		self.top.SetStatusText("Downloading engines...")

		engines = self.clb_versions.GetCheckedStrings()
		for e in engines:
			try:
				should_download = True
				epath = "engines/"+e
				if os.path.exists(epath):
					avail_sha256 = urllib.request.urlopen(self.url+"/engines.php?sha256="+e).read().decode("utf-8")
					if not avail_sha256:
						raise ValueError("Invalid URL path: ?sha256="+e)

					with open(epath, "rb") as f:
						current_sha256 = hashlib.sha256(f.read()).hexdigest()

					if avail_sha256 == current_sha256:
						should_download = False

				if should_download:
					urllib.request.urlretrieve(self.url+"/"+e, epath+".part", progress)
					os.rename(epath+".part", epath)
			except Exception as e:
				print(e)
				d = wx.MessageDialog(self.top, "Failed to fetch marked engines", style=wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP | wx.CENTER)
				d.ShowModal()
				d.Destroy()
				self.top.SetStatusText("")
				return

		self.top.SetStatusText("")
		d = wx.MessageDialog(self.top, "Finished downloading the marked engines", style=wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP | wx.CENTER)
		d.ShowModal()
		d.Destroy()
	def Close(self, e):
		self.EndModal(0)

	def getAvailableEngines(self) -> list:
		try:
			avail_engines = urllib.request.urlopen(self.url+"/engines.php")
		except urllib.error.URLError as e:
			print(e)
			return None
		if not avail_engines:
			print("No engines returned")
			return None
		return json.load(avail_engines)
	def getCurrentEngines(self) -> list:
		current_engines = []
		if os.path.exists("engines/linux"):
			current_engines.extend(["linux/"+ce for ce in os.listdir("engines/linux") if not ce.endswith(".part")])
		if os.path.exists("engines/win"):
			current_engines.extend(["win/"+ce for ce in os.listdir("engines/win") if not ce.endswith(".part")])
		if os.path.exists("engines/macos"):
			current_engines.extend(["macos/"+ce for ce in os.listdir("engines/macos") if not ce.endswith(".part")])
		return current_engines

	def show(self):
		# Update fields
		self.ga_progress.SetRange(100)
		self.ga_progress.SetValue(0)

		self.top.SetStatusText("Loading available engines...")
		avail_engines = self.getAvailableEngines()
		if not avail_engines:
			d = wx.MessageDialog(self.top, "Failed to fetch available engines", style=wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP | wx.CENTER)
			d.ShowModal()
			d.Destroy()
			self.top.SetStatusText("")
			return

		self.clb_versions.Clear()
		for ae in avail_engines:
			self.clb_versions.Append(ae)
		self.Fit()

		# Workaround ignored window positioning
		self.Show(True)
		self.Center()
		self.Show(False)

		self.clb_versions.SetCheckedStrings(self.getCurrentEngines())

		self.ShowModal()
