# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

from core.bash import BashInterpreter

from ui.editor import BEEFEditor

class BEEFConsole(wx.Dialog):
	def __init__(self, top, parent, wd):
		wx.Dialog.__init__(self)
		self.top = top
		self.wd = wd
		self.isShown = False

		display = wx.ClientDisplayRect()
		size = (850, 400)
		self.pos = (display[2]-size[0], display[3]-size[1])

		self.Create(parent, -1, "Compile Log", self.pos, size)

		sizer = wx.BoxSizer(wx.VERTICAL)

		self.log = BEEFEditor(self, size=size)
		self.log.SetMarginType(1, 0)
		self.log.SetMarginWidth(1, 0)
		self.log.SetEdgeMode(wx.stc.STC_EDGE_NONE)
		self.log.SetWrapMode(True)
		sizer.Add(self.log, 0, wx.ALL, 5)

		self.SetSizer(sizer)
		sizer.Fit(self)

		self.Bind(wx.EVT_CLOSE, self.toggle)

		# Workaround ignored window position in Create()
		self.Show(True)
		self.Move(self.pos)
		self.Show(False)

	def clear(self):
		self.log.SetText("")
	def show(self, s=True):
		if s:
			# Workaround ignored window position in Create()
			self.Show(True)
			self.Move(self.pos)
			self.Show(False)

		self.isShown = s
		self.Show(self.isShown)
		self.top.Raise()
		self.top.toolbar.ToggleTool(70, self.isShown)

		if not s:
			self.pos = self.GetScreenPosition()
	def toggle(self, event=None):
		self.show(not self.isShown)

	def update(self):
		self.log.DocumentEnd()
		self.log.SetMarginType(1, 0)
		self.log.SetMarginWidth(1, 0)

		self.Refresh()
		self.Update()
	def writeOut(self, s):
		if s:
			self.log.AppendText(s)
			self.update()
	def writeErr(self, s):
		if s:
			self.log.AppendText(s)
			self.update()

	def call(self, cmd):
		shell = BashInterpreter(self, self.wd, cmd)
		self.writeOut("> " + " ".join(cmd) + "\n")
		r = shell.run("")
		shell.cleanup()
		return r
