# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import wx.aui

from copy import copy

class BEEFNotebook(wx.aui.AuiNotebook):
	def __init__(self, top, parent):
		wx.aui.AuiNotebook.__init__(self, parent, style=wx.aui.AUI_NB_TOP | wx.aui.AUI_NB_TAB_SPLIT | wx.aui.AUI_NB_TAB_MOVE | wx.aui.AUI_NB_SCROLL_BUTTONS | wx.aui.AUI_NB_CLOSE_ON_ALL_TABS)
		self.top = top
		self.parent = parent

		self.resources = {}
		self.is_updating = False

		self.top.Bind(wx.aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.onPageChanged)
		self.top.Bind(wx.aui.EVT_AUINOTEBOOK_TAB_RIGHT_UP, self.onRightUp)
		self.top.Bind(wx.EVT_MENU, self.splitRight, id=10010)
		self.top.Bind(wx.EVT_MENU, self.splitDown, id=10011)
		self.top.Bind(wx.aui.EVT_AUINOTEBOOK_PAGE_CLOSED, self.destroyPage)

		self.top.Bind(wx.EVT_MENU, self.nextPage, id=10012)
		self.top.Bind(wx.EVT_MENU, self.prevPage, id=10013)
		accelTable = wx.AcceleratorTable([
			(wx.ACCEL_CTRL, wx.WXK_PAGEDOWN, 10012),
			(wx.ACCEL_CTRL, wx.WXK_PAGEUP, 10013)
		])
		self.SetAcceleratorTable(accelTable)

	def AddPage(self, res, page, caption, select=False, bitmap=wx.NullBitmap):
		r = wx.aui.AuiNotebook.AddPage(self, page, caption, select, bitmap)
		if r:
			self.resources[res.pageIndex] = res
		return r
	def destroyPage(self, event):
		item = event.GetSelection()
		res = self.resources[item]

		del self.resources[item]
		res.destroyPage(event)
	def onPageChanged(self, event):
		item = event.GetSelection()
		if not item in self.resources:
			return
		res = self.resources[item]

		res.focusPage()
	def onRightUp(self, event):
		item = event.GetSelection()
		res = self.resources[item]
		cmenu = wx.Menu()

		cmenu.Append(10010, "Split right")
		cmenu.Append(10011, "Split down")

		res.page.PopupMenu(cmenu)
		cmenu.Destroy()

	def _split(self, event):
		item = self.GetSelection()
		res = self.resources[item]

		new_res = copy(res)
		self.is_updating = True
		new_res.initPage()
		new_res.update(res)
		self.is_updating = False

		return new_res
	def splitRight(self, event):
		new_res = self._split(event)
		self.top.notebook.Split(new_res.pageIndex, wx.RIGHT)
	def splitDown(self, event):
		new_res = self._split(event)
		self.top.notebook.Split(new_res.pageIndex, wx.DOWN)

	def nextPage(self, event):
		self.top.notebook.AdvanceSelection()
	def prevPage(self, event):
		self.top.notebook.AdvanceSelection(False)

	def adjustIndices(self, index):
		if index < 0:
			return

		resources = {}
		for (p, r) in self.resources.items():
			if r.pageIndex > index:
				r.pageIndex -= 1
			resources[r.pageIndex] = r
		self.resources = resources
	def updateResource(self, res):
		if self.is_updating:
			return

		self.is_updating = True
		for (p, r) in self.resources.items():
			if r.type == res.type and r.id == res.id and r is not res:
				r.update(res)
		self.is_updating = False
