# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

def getInt(s):
	return int(round(float(s)))

class BEEFPlot(wx.Panel):
	def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.LC_ICON, scale=-1):
		wx.Panel.__init__(self, parent, id, pos, size, style)
		self.SetBackgroundColour(wx.WHITE)

		self.scale = scale
		self.coords = []

		self.Bind(wx.EVT_SIZE, self.OnSize)
		self.Bind(wx.EVT_PAINT, self.Redraw)

	def SetCoords(self, coords):
		self.coords = []
		for c in coords:
			self.coords.append(c)

	def OnSize(self, event):
		self.Redraw()
	def Redraw(self, event=None):
		if not event:
			self.Refresh()
			self.Update()
			return

		size = self.GetSize()
		size = (size.GetWidth(), size.GetHeight())
		ox = size[0]/2
		oy = size[1]/2

		xs = [float(c[0]) for c in self.coords] + [0.0]
		ys = [float(c[1]) for c in self.coords] + [0.0]
		min_coord = min(xs + ys)
		max_coord = max(xs + ys)

		scale = self.scale
		if scale == -1:
			if min_coord == 0.0 and max_coord == 0.0:
				scale = 2
			else:
				d = abs(max_coord - min_coord) / 10
				min_coord -= d
				max_coord += d
				ox = oy = max(abs(min_coord), abs(max_coord))
				scale = max(size) / ox * (2/3)

		# Create lists for drawing
		w = 2/scale
		points = []
		lines = []

		for i in range(len(self.coords)):
			c1 = self.coords[i]
			points.append((
				getInt(c1[0])-w/2, getInt(c1[1])-w/2,
				w, w
			))

		lines.append((-ox, 0, ox, 0)) # Axis lines
		lines.append((0, -oy, 0, oy))
		for i in range(len(self.coords)):
			if i+1 >= len(self.coords):
				break

			c1 = self.coords[i]
			c2 = self.coords[i+1]

			lines.append((
				getInt(c1[0]), getInt(c1[1]),
				getInt(c2[0]), getInt(c2[1]),
			))

		# Draw the lists
		dc = wx.PaintDC(self)
		dc.Clear()

		dc.SetLogicalScale(scale, scale)
		dc.SetLogicalOrigin(min_coord, min_coord)

		dc.SetPen(wx.Pen(wx.BLACK, w))
		dc.DrawLineList(lines)
		dc.SetPen(wx.Pen(wx.RED, w))
		dc.DrawRectangleList(points)
