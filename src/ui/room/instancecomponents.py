# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

import wx.lib.itemspicker as itemspicker

class BEEFRoomInstanceComponents(wx.Dialog):
	def __init__(self, top):
		wx.Dialog.__init__(self)
		self.Create(top, -1, "Edit Instance Components", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		self.top = top

		gbs = wx.GridBagSizer(2, 4)

		gbs.Add(wx.StaticText(self, -1, "All Components:"), (0, 0), (1, 1))
		gbs.Add(wx.StaticText(self, -1, "Selected Components:"), (0, 3), (1, 1))
		self.ip_components = itemspicker.ItemsPicker(self, -1, [], size=(500,200))
		gbs.Add(self.ip_components, (1, 0), (1, 4))

		buttons = wx.StdDialogButtonSizer()
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		buttons.Add(btn)

		btn = wx.Button(self, wx.ID_CANCEL)
		buttons.Add(btn)
		buttons.Realize()
		gbs.Add(buttons, (2, 0), (1, 2))

		sizer = wx.BoxSizer()
		sizer.Add(gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.SetSizer(sizer)
		sizer.Fit(self)

		self.CenterOnScreen()

	def show(self, components):
		# Update fields
		self.ip_components.SetItems([o.name for o in self.top.objects])
		self.ip_components.SetSelections(components)

		val = self.ShowModal()
		if val == wx.ID_OK:
			new_components = self.ip_components.GetSelections()
			if new_components != components:
				self.top.setUnsaved()
			return new_components
		return components
