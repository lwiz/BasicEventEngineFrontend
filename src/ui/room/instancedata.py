# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

from ui.listctrl import BEEFListCtrl

class BEEFRoomInstanceData(wx.Dialog):
	def __init__(self, top):
		wx.Dialog.__init__(self)
		self.Create(top, -1, "Edit Instance Data", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		self.top = top

		gbs = wx.GridBagSizer(2, 2)

		gbs.Add(wx.StaticText(self, -1, "Instance Data:"), (0, 0), (1, 1))

		self.lst_data = BEEFListCtrl(self, style=wx.LC_REPORT | wx.LC_EDIT_LABELS | wx.EXPAND)
		for c in ["Name", "Value"]:
			i = self.lst_data.AppendColumn(c)
		self.lst_data.SetColumnWidth(0, 150)
		self.lst_data.SetColumnWidth(1, 200)
		self.lst_data.SetValidators()
		def _onListEdit(evt, source=self.lst_data):
			return self.onListEdit(evt, source)
		self.Bind(wx.EVT_LIST_END_LABEL_EDIT, _onListEdit, self.lst_data)

		gbs.Add(self.lst_data, (1, 0), (1, 2), flag=wx.EXPAND)

		buttons = wx.StdDialogButtonSizer()
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		buttons.Add(btn)

		btn = wx.Button(self, wx.ID_CANCEL)
		buttons.Add(btn)
		buttons.Realize()
		gbs.Add(buttons, (2, 0), (1, 2))

		sizer = wx.BoxSizer()
		sizer.Add(gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.SetSizer(sizer)
		sizer.Fit(self)

		self.CenterOnScreen()

	def onListEdit(self, event, source):
		item = event.GetItem()
		pos = item.GetId()
		if self.lst_data.GetItemData(pos) == 0 and item.GetText():
			self.lst_data.SetItemData(pos, 1)
			self.lst_data.Append(("", ""))

	def show(self, data):
		# Update fields
		self.lst_data.DeleteAllItems()
		for k, v in data.items():
			pos = self.lst_data.Append((k, v))
			self.lst_data.SetItemData(pos, 1)
		self.lst_data.Append(("", ""))

		val = self.ShowModal()
		if val == wx.ID_OK:
			new_data = {}
			for item in self.lst_data.GetItemList():
				if item[0]:
					new_data[item[0]] = item[1]

			if new_data != data:
				self.top.setUnsaved()
			return new_data
		return data
