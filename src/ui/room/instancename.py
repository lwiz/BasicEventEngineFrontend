# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

class BEEFRoomInstanceName(wx.Dialog):
	def __init__(self, top):
		wx.Dialog.__init__(self)
		self.Create(top, -1, "Edit Instance Name", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		self.top = top

		gbs = wx.GridBagSizer(2, 2)

		gbs.Add(wx.StaticText(self, -1, "Instance Name:"), (0, 0), (1, 1))
		self.text_inst_name = wx.TextCtrl(self, -1, "", size=(180,-1))
		gbs.Add(self.text_inst_name, (1, 0), (1, 2))

		buttons = wx.StdDialogButtonSizer()
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		buttons.Add(btn)

		btn = wx.Button(self, wx.ID_CANCEL)
		buttons.Add(btn)
		buttons.Realize()
		gbs.Add(buttons, (2, 0), (1, 2))

		sizer = wx.BoxSizer()
		sizer.Add(gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.SetSizer(sizer)
		sizer.Fit(self)

		self.CenterOnScreen()

	def show(self, inst_name):
		# Update fields
		self.text_inst_name.SetValue(inst_name)

		val = self.ShowModal()
		if val == wx.ID_OK:
			new_name = self.text_inst_name.GetValue()
			if new_name != inst_name:
				self.top.setUnsaved()
			return new_name
		return inst_name
