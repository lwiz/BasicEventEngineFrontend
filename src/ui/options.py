# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

class BEEFOptions(wx.Dialog):
	def __init__(self, top):
		wx.Dialog.__init__(self)
		self.Create(top, -1, "Edit Options", wx.DefaultPosition, wx.DefaultSize, wx.DEFAULT_DIALOG_STYLE)
		self.top = top

		gbs = wx.GridBagSizer(12, 2)

		gbs.Add(wx.StaticText(self, -1, "Game Name:"), (0, 0), (1, 1))
		self.text_game_name = wx.TextCtrl(self, -1, "", size=(180,-1))
		gbs.Add(self.text_game_name, (1, 0), (1, 2))

		gbs.Add(wx.StaticText(self, -1, "Game Version:"), (2, 0), (1, 1))
		self.text_game_version_major = wx.TextCtrl(self, -1, "", size=(60,-1))
		gbs.Add(self.text_game_version_major, (3, 0), (1, 1))
		self.text_game_version_minor = wx.TextCtrl(self, -1, "", size=(60,-1))
		gbs.Add(self.text_game_version_minor, (3, 1), (1, 1))
		self.text_game_version_patch = wx.TextCtrl(self, -1, "", size=(60,-1))
		gbs.Add(self.text_game_version_patch, (3, 2), (1, 1))

		buttons = wx.StdDialogButtonSizer()
		btn = wx.Button(self, wx.ID_OK)
		btn.SetDefault()
		buttons.Add(btn)

		btn = wx.Button(self, wx.ID_CANCEL)
		buttons.Add(btn)
		buttons.Realize()
		gbs.Add(buttons, (4, 0), (1, 2))

		sizer = wx.BoxSizer()
		sizer.Add(gbs, 1, wx.ALL | wx.EXPAND, 20)
		self.SetSizer(sizer)
		sizer.Fit(self)

		self.CenterOnScreen()

	def show(self):
		# Update fields
		self.text_game_name.SetValue(self.top.gameCfg["game_name"])
		self.text_game_version_major.SetValue(str(self.top.gameCfg["game_version_major"]))
		self.text_game_version_minor.SetValue(str(self.top.gameCfg["game_version_minor"]))
		self.text_game_version_patch.SetValue(str(self.top.gameCfg["game_version_patch"]))

		val = self.ShowModal()
		if val == wx.ID_OK:
			self.top.gameCfg["game_name"] = self.text_game_name.GetValue()
			self.top.gameCfg["game_version_major"] = int(self.text_game_version_major.GetValue())
			self.top.gameCfg["game_version_minor"] = int(self.text_game_version_minor.GetValue())
			self.top.gameCfg["game_version_patch"] = int(self.text_game_version_patch.GetValue())

			self.top.setUnsaved()
