# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

try:
	import wx
except ImportError:
	raise ImportError("The wxPython module is required to run this program")

from ui.room.instancecomponents import BEEFRoomInstanceComponents
from ui.room.instancename import BEEFRoomInstanceName
from ui.room.instancedata import BEEFRoomInstanceData

class BEEFGrid(wx.Panel):
	def __init__(self, parent, top, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.LC_ICON, scale=1):
		wx.Panel.__init__(self, parent, id, pos, size, style)
		self.SetBackgroundColour(wx.WHITE)

		self.top = top

		self.scale = scale
		self.object = None
		self.instances = []
		self.selected_inst = None

		self.w = 0
		self.h = 0
		self.gridsize = 16

		self.Bind(wx.EVT_LEFT_UP, self.CreateInstance)
		self.Bind(wx.EVT_RIGHT_UP, self.ModifyInstance)
		self.Bind(wx.EVT_MIDDLE_UP, self.DeleteInstance)
		self.Bind(wx.EVT_SIZE, self.OnSize)
		self.Bind(wx.EVT_PAINT, self.Redraw)

		self.top.Bind(wx.EVT_MENU, self.CMenuModifyComponents, id=10020)
		self.top.Bind(wx.EVT_MENU, self.CMenuModifyName, id=10021)
		self.top.Bind(wx.EVT_MENU, self.CMenuModifyData, id=10022)
		self.top.Bind(wx.EVT_MENU, self.DeleteInstance, id=10023)

		self.instancecomponents = BEEFRoomInstanceComponents(self.top)
		self.instancename = BEEFRoomInstanceName(self.top)
		self.instancedata = BEEFRoomInstanceData(self.top)

	def SetObject(self, object):
		self.object = object
	def AddInstance(self, inst):
		self.instances.append(inst)
	def ClearInstances(self, inst):
		self.instances = []
	def SetDimensions(self, w, h):
		self.w = w
		self.h = h

	def GetEventPos(self, event):
		gx, gy = self.GetPosition()
		x = event.GetX()/self.scale
		y = event.GetY()/self.scale

		# Align to grid
		x = x//self.gridsize * self.gridsize
		y = y//self.gridsize * self.gridsize

		return (x, y)
	def getInstanceSprites(self, inst):
		components = [o for o in self.top.objects if o.name in inst[0]]
		if components:
			return [s
				for s in self.top.textures
					if s.name == components[0].properties["sprite"]
						and s.properties["path"]
			]
		return []
	def hitInstance(self, x, y, inst):
		w = 16
		h = 16

		sprites = self.getInstanceSprites(inst)
		if sprites:
			w = sprites[0].properties["width"] // sprites[0].properties["subimage_amount"]
			h = sprites[0].properties["height"]

		if x >= inst[1][0] and y >= inst[1][1] and x <= inst[1][0]+w and y <= inst[1][1]+h:
			return True
		return False
	def CreateInstance(self, event):
		if not self.object:
			return

		x, y = self.GetEventPos(event)
		self.AddInstance(([self.object], (x, y, 0.0), {}))
		self.top.setUnsaved()

		self.Redraw()
	def DeleteInstance(self, event):
		if not self.selected_inst:
			x, y = self.GetEventPos(event)
			for inst in list(self.instances):
				if self.hitInstance(x, y, inst):
					self.selected_inst = inst
					break

		self.instances.remove(self.selected_inst)
		self.top.setUnsaved()
		self.selected_inst = None

		self.Redraw()
	def _modify(self, x, y, inst):
		cmenu = wx.Menu()
		cmenu.Append(10020, "Edit Instance components")
		cmenu.Append(10021, "Edit Instance name")
		cmenu.Append(10022, "Edit Instance creation data")
		cmenu.Append(10023, "Delete Instance")

		self.selected_inst = inst
		self.PopupMenu(cmenu)
		cmenu.Destroy()
	def ModifyInstance(self, event):
		x, y = self.GetEventPos(event)
		for inst in list(self.instances):
			if self.hitInstance(x, y, inst):
				self._modify(x, y, inst)
				break

		self.Redraw()
	def OnSize(self, event):
		self.Redraw()
	def Redraw(self, event=None):
		if not event:
			self.Refresh()
			self.Update()
			return

		dc = wx.PaintDC(self)
		dc.Clear()

		dc.SetLogicalScale(self.scale, self.scale)
		dc.SetLogicalOrigin(0, 0)

		# Draw gridlines
		gridlines = []
		for x in range(0, self.w, self.gridsize):
			gridlines.append((
				x, 0,
				x, self.h
			))
		for y in range(0, self.h, self.gridsize):
			gridlines.append((
				0, y,
				self.w, y
			))
		dc.SetPen(wx.Pen(wx.Colour(192, 192, 192), 1))
		dc.DrawLineList(gridlines)

		dc.SetPen(wx.Pen(wx.BLACK, 1))
		dc.DrawLineList([(0, 0, 0, self.h), (0, 0, self.w, 0)])

		# Draw the instances
		for inst in self.instances:
			possible_textures = self.getInstanceSprites(inst)
			sprite = wx.Bitmap(self.top.images["nosprite"])
			if possible_textures:
				texture = possible_textures[0]
				if "bmp_texture" not in texture.inputs:
					# Force load the bitmap
					print("Force loading instance sprite...")
					is_unsaved = self.top.getUnsaved()
					texture.initPage()
					texture.destroyPage()
					self.top.setUnsaved(is_unsaved)

				sprite = texture.inputs["bmp_texture"].GetBitmap()

			dc.DrawBitmap(sprite, inst[1][0], inst[1][1], True)

	def CMenuModifyComponents(self, event):
		self.selected_inst[0][:] = self.instancecomponents.show(self.selected_inst[0])
	def CMenuModifyName(self, event):
		name = ""
		if "name" in self.selected_inst[2]:
			name = self.selected_inst[2]["name"]
		self.selected_inst[2]["name"] = self.instancename.show(name)
	def CMenuModifyData(self, event):
		data = {}
		if "data" in self.selected_inst[2]:
			data = self.selected_inst[2]["data"]
		self.selected_inst[2]["data"] = self.instancedata.show(data)
