# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

import sys
import subprocess
import threading
import queue
import platform

class BashThread(threading.Thread):
	def __init__(self, readline):
		threading.Thread.__init__(self)

		self.readline = readline
		self.output = queue.Queue()

		self.stop_event = threading.Event()
		self.setDaemon(True)
	def run(self):
		while not self.stopped():
			line = self.readline()
			self.output.put(line)
	def stop(self):
		self.stop_event.set()
	def stopped(self):
		return self.stop_event.is_set()
	def getOutput(self):
		lines = []
		while True:
			try:
				line = self.output.get_nowait()
				line = line.decode("utf-8")
				lines.append(line)
			except queue.Empty:
				break
			except UnicodeDecodeError:
				lines.append("".join(map(chr, line)))
		return "".join(lines)

class BashInterpreter:
	def __init__(self, output, wd, command=None):
		self.output = output

		self.wd = wd

		if not command:
			command = ["bash"]
		if platform.system() == "Windows":
			self.bash = subprocess.Popen(" ".join(command), shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.wd)
		else:
			self.bash = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.wd)

		self.outThread = BashThread(lambda: self.bash.stdout.read(1))
		self.outThread.start()

		self.errorThread = BashThread(lambda: self.bash.stderr.read(1))
		self.errorThread.start()
	def run(self, command, shouldPrint=False):
		if command.strip():
			self.bash.stdin.write((command+"\n").encode())
			self.bash.stdin.flush()
			if shouldPrint and self.output:
				self.output.writeOut("> " + command + "\n")

		while self.bash.poll() is None:
			o = self.outThread.getOutput()
			e = self.errorThread.getOutput()
			if self.output:
				if o:
					self.output.writeOut(o)
				if e:
					self.output.writeErr(e)

		return self.bash.returncode
	def cleanup(self):
		self.outThread.stop()
		self.errorThread.stop()

		self.outThread.join()
		self.errorThread.join()

def call_bash(wd, cmd):
	class BashOutput:
		def __init__(self):
			self.output = ""
			self.error = ""
		def writeOut(self, str):
			self.output += str
		def writeErr(self, str):
			self.error += str

	output = BashOutput()
	shell = BashInterpreter(output, wd, cmd)
	r = shell.run("")
	shell.cleanup()
	return (r, output.output, output.error)
