# Copyright (c) 2017-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEEF.
# BEEF is free software and comes with ABSOLUTELY NO WARRANTY.
# See LICENSE for more details.

import os
import errno
import shutil
import json
import math
import re
import platform
import subprocess
import datetime
import tarfile
import zipfile
import glob

from resource.enum import ECompile
from resource.enum import ELight

def copytree(src, dst, symlinks=False):
	names = os.listdir(src)
	if not os.path.isdir(dst):
		os.makedirs(dst)
	errors = []
	for name in names:
		srcname = os.path.join(src, name)
		dstname = os.path.join(dst, name)
		try:
			if symlinks and os.path.islink(srcname):
				linkto = os.readlink(srcname)
				os.symlink(linkto, dstname)
			elif os.path.isdir(srcname):
				copytree(srcname, dstname, symlinks)
			else:
				shutil.copy2(srcname, dstname)
			# XXX What about devices, sockets etc.?
		except OSError as why:
			errors.append((srcname, dstname, str(why)))
	try:
		shutil.copystat(src, dst)
	except OSError as why:
		# can't copy file access times on Windows
		if why.winerror is None:
			errors.extend((src, dst, str(why)))
	if errors:
		raise Error(errors)

class Compiler:
	def __init__(self, top):
		self.top = top
		self.buildDir = "{}/build".format(self.top.getDir())
		self.srcDir = "{}/engine-debug".format(self.buildDir)
		self.resDir = "{}/main/resources".format(self.srcDir)

	def generate(self, type):
		if not self.top.prefCfg["default_engine"]:
			self.top.SetStatusText("Can't run without a default engine, set one in File > Preferences")
			return 1

		engine = self.top.prefCfg["default_engine"]
		self.top.SetStatusText("Extracting engine...")
		print("Extracting engine {}...".format(engine))

		sdname = "engine-{}".format("debug" if type == ECompile.DEBUG else "release")
		last_engine = None
		try:
			with open("{}/{}.txt".format(self.buildDir, sdname)) as f:
				last_engine = f.readline().rstrip("\n")
		except FileNotFoundError:
			pass
		if engine != last_engine:
			if os.path.exists("{}/{}".format(self.buildDir, sdname)):
				shutil.rmtree("{}/{}".format(self.buildDir, sdname))
			shutil.unpack_archive("engines/"+engine, self.buildDir)

		with open("{}/{}.txt".format(self.buildDir, sdname), "w") as f:
			f.write(engine)

		self.srcDir = "{}/{}".format(self.buildDir, sdname)
		self.top.SetStatusText("Generating resources...")
		print("Generating {} resources...".format(self.top.gameCfg["game_name"]))
		self.resDir = "{}/main/resources".format(self.srcDir)

		self._createDir(self.resDir)
		self._generate()
		self._copyConfigs()
		self._copyExtras()
		self._copyResources()

		return 0
	def run(self):
		print("Running {}...".format(self.top.gameCfg["game_name"]))

		if platform.system() == "Windows":
			rc = self.top.console.call(["start.bat"])
		else:
			rc = self.top.console.call(["./engine"])
	def debug(self):
		print("Debugging {}...".format(self.top.gameCfg["game_name"]))
		self.top.SetStatusText("Debugging, see the console that launched BEEF for input")
		cproc = subprocess.run(["gdb", "./engine"], cwd=self.srcDir)
	def clean(self, is_user=False):
		print("Cleaning old generated files...")

		if is_user:
			shutil.rmtree(self.buildDir)
			return

		sdnames = ["engine-debug", "engine-release"]
		for sdname in sdnames:
			sdpath = "{}/{}".format(self.buildDir, sdname)
			if os.path.exists(sdpath+"/main"):
				shutil.rmtree(sdpath+"/main")
				for f in ["config.json", "gmon.out"]:
					if os.path.exists(sdpath+"/"+f):
						os.remove(sdpath+"/"+f)
				for f in glob.glob(sdpath+"/*.log"):
					os.remove(f)
	def package(self):
		print("Packaging {}...".format(self.top.gameCfg["game_name"]))

		sdname = "engine-release"
		last_engine = None
		try:
			with open("{}/{}.txt".format(self.buildDir, sdname)) as f:
				last_engine = f.readline().rstrip("\n")
		except FileNotFoundError:
			pass
		if not last_engine:
			return

		pkgDir = self.top.rootDir+"/pkg"
		self._createDir(pkgDir)
		today = datetime.date.today()
		platarch = last_engine[:last_engine.find("/")] + "64"
		pkgName = "{}-{}.{}.{}-build{:04}.{:02}.{:02}-{}".format(
			re.sub(r"\W+", "", self.top.gameCfg["game_name"]),
			self.top.gameCfg["game_version_major"],
			self.top.gameCfg["game_version_minor"],
			self.top.gameCfg["game_version_patch"],
			today.year,
			today.month,
			today.day,
			platarch
		)
		if last_engine.startswith("linux") or last_engine.startswith("macos"):
			with tarfile.open("{}/{}.tar.xz".format(pkgDir, pkgName), "w:xz") as f:
				f.add(self.srcDir, re.sub(r"\W+", "", self.top.gameCfg["game_name"]))
		else:
			gameRoot = re.sub(r"\W+", "", self.top.gameCfg["game_name"])
			with zipfile.ZipFile("{}/{}.zip".format(pkgDir, pkgName), "w", zipfile.ZIP_DEFLATED) as f:
				for root, dirs, files in os.walk(self.srcDir):
					for file in files:
						f.write("{}/{}".format(root, file), os.path.join(gameRoot, os.path.relpath(os.path.join(root, file), self.srcDir)))

		self._openDir(pkgDir)

	def _createDir(self, path):
		try:
			os.makedirs(path)
		except OSError as e:
			if e.errno != errno.EEXIST:
				raise
	def _openDir(self, path):
		if platform.system() == "Windows":
			os.startfile(path)
		elif platform.system() == "Darwin":
			subprocess.Popen(["open", path])
		else:
			subprocess.Popen(["xdg-open", path])

	def _generate(self):
		shutil.rmtree(self.resDir)

		self._generatePaths()
		self._generateTimelines()
		self._generateLights()
		self._generateScripts()
		self._generateObjects()
		self._generateRooms()
		self._generateResources()
		self._generateConfig()
	def _generatePaths(self):
		self._createDir(self.resDir+"/paths")

		for p in self.top.paths:
			with open("{}/paths/{}.nodes.json".format(self.resDir, p.name), "w") as pathFile:
				nodes = [[[float(n[0][0]), float(n[0][1]), float(n[0][2])], float(n[1])] for n in p.properties["nodes"]]
				#cps = {int(n): [float(cp[0]), float(cp[1]), float(cp[2])] for (n, cp) in p.properties["control_points"].items()}
				cps = []
				for (n, cp) in p.properties["control_points"].items():
					n = int(n)
					cpc = [float(cp[0]), float(cp[1]), float(cp[2])]
					is_all_nan = 0
					if math.isnan(cpc[0]):
						cpc[0] = nodes[n][0][0]
						is_all_nan += 1
					if math.isnan(cpc[1]):
						cpc[1] = nodes[n][0][1]
						is_all_nan += 1
					if math.isnan(cpc[2]):
						cpc[2] = nodes[n][0][2]
						is_all_nan += 1
					if is_all_nan < 3: # Don't use the control point if none of the coordinates are valid
						cps.append([n, cpc])

				ncp = {"nodes": nodes, "control_points": cps}
				pathFile.write(json.dumps(ncp, indent=4))
	def _generateTimelines(self):
		self._createDir(self.resDir+"/timelines")

		for t in self.top.timelines:
			with open("{}/timelines/{}.py".format(self.resDir, t.name), "w") as timelineFile:
				actions = ""
				adds = "tl = bee.Timeline(\"{}\")\n".format(t.name)
				for a in t.properties["actions"]:
					actions += "def ac{}_{}(name):\n{}\n\tpass\n".format(a[0], a[1], "".join(["\t"+line for line in a[2].splitlines(True)]))
					adds += "tl.add_action({0}, \"{1}\", ac{0}_{1})\n".format(a[0], a[1])
				actions += "def ac_end(name):\n{}\n\tpass\n".format("".join(["\t"+line for line in t.properties["end_action"].splitlines(True)]))
				adds += "tl.set_ending(ac_end)\n"

				timelineFile.write("import bee\n\n" + actions + "\n\n" + adds)
	def _generateLights(self):
		self._createDir(self.resDir+"/lights")

		for l in self.top.lights:
			with open("{}/lights/{}.data.json".format(self.resDir, l.name), "w") as lightFile:
				lp = l.properties
				ldata = {}
				ldata["type"] = ELight.str(lp["type"])
				ldata["position"] = [float(lp["position"][0]), float(lp["position"][1]), float(lp["position"][2]), 0.0]
				ldata["direction"] = [float(lp["direction"][0]), float(lp["direction"][1]), float(lp["direction"][2]), 1.0]
				ldata["attenuation"] = [float(lp["attenuation"][0]), float(lp["attenuation"][1]), float(lp["attenuation"][2]), float(lp["attenuation"][3])]
				ldata["color"] = [int(lp["color"][0]), int(lp["color"][1]), int(lp["color"][2]), int(lp["color"][3])]

				lightFile.write(json.dumps(ldata, indent=4))
	def _generateScripts(self):
		self._createDir(self.resDir+"/scripts")

		for s in self.top.scripts:
			with open("{}/scripts/{}.py".format(self.resDir, s.name), "w") as scriptFile:
				scriptFile.write(s.properties["code"])
	def _generateObjects(self):
		self._createDir(self.resDir+"/objects")

		for o in self.top.objects:
			with open("{}/objects/{}.py".format(self.resDir, o.name), "w") as objFile:
				objFile.write(o.properties["code"])
	def _generateRooms(self):
		self._createDir(self.resDir+"/rooms")

		roomTemplate = ""
		with open("templates/room.py", "r") as roomTemplateFile:
			roomTemplate = roomTemplateFile.read()

		for r in self.top.rooms:
			room = roomTemplate.format(
				width=r.properties["width"],
				height=r.properties["height"],
				gravity=r.properties["gravity"],
				scale=r.properties["scale"],
				backgrounds=r.getBackgroundInit(),
				views=r.getViewPortInit(),
				initCode=r.getInitCode(),
				startCode=r.getStartCode(),
				endCode=r.getEndCode()
			)
			with open("{}/rooms/{}.py".format(self.resDir, r.name), "w") as roomFile:
				roomFile.write(room)
			with open("{}/rooms/{}.insts.json".format(self.resDir, r.name), "w") as instanceFile:
				instanceFile.write("[\n\t" + ",\n\t".join(r.getInstanceMap()) + "\n]\n")
	def _generateResources(self):
		firstRoom = self.top.gameCfg["first_room"]
		if not firstRoom:
			if len(self.top.rooms) == 0:
				self.top.SetStatusText("No Rooms to run")
				raise ValueError("No Rooms to run")
			firstRoom = self.top.rooms[0].name

		resources = ""
		with open("templates/resources.json", "r") as resourcesTemplate:
			resources = resourcesTemplate.read()
		resources = resources.format(
			textures=",\n".join([t.getRes() for t in self.top.textures if t.properties["path"]]), # TODO use JSON
			sounds=",\n".join([s.getRes() for s in self.top.sounds if s.properties["path"]]),
			fonts=",\n".join([f.getRes() for f in self.top.fonts if f.properties["path"]]),
			paths=",\n".join([p.getRes() for p in self.top.paths]),
			timelines=",\n".join([t.getRes() for t in self.top.timelines]),
			meshes=",\n".join([m.getRes() for m in self.top.meshes if m.properties["path"]]),
			lights=",\n".join([l.getRes() for l in self.top.lights]),
			scripts=",\n".join([s.getRes() for s in self.top.scripts]),
			objects=",\n".join([o.getRes() for o in self.top.objects]),
			rooms=",\n".join([r.getRes() for r in self.top.rooms]),
			firstRoom=firstRoom
		)
		with open(self.srcDir+"/main/resources.json", "w") as resourcesFile:
			resourcesFile.write(resources)
	def _generateConfig(self):
		config = ""
		with open("templates/config.json", "r") as configTemplate:
			config = configTemplate.read()
		config = config.format(
			game=re.sub(r"\W+", "", self.top.gameCfg["game_name"]),
			v_major=self.top.gameCfg["game_version_major"],
			v_minor=self.top.gameCfg["game_version_minor"],
			v_patch=self.top.gameCfg["game_version_patch"]
		)
		with open(self.srcDir+"/config.json", "w") as configFile:
			configFile.write(config)

	def _copyConfigs(self):
		cfgdir = "{}/main/cfg".format(self.srcDir)
		if os.path.exists(cfgdir):
			shutil.rmtree(cfgdir)
		self._createDir(cfgdir)
		if self.top.configs:
			copytree("{}/cfg".format(self.top.rootDir), cfgdir)
	def _copyExtras(self):
		copytree("{}/resources/extras".format(self.top.rootDir), "{}/extras".format(self.resDir))
	def _copyResources(self):
		copytree("{}/resources".format(self.top.rootDir), self.resDir)
