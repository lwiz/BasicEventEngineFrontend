###############################################################################
# Copyright (c) 2015-20 Louise Montalvo <louanmontalvo@gmail.com>
#
# This file is part of BEE.
# BEE is free software and comes with ABSOLUTELY NO WARRANTY
# See LICENSE for more details.
###############################################################################

import bee

def init(self):
    # Set Room properties
    self.set_width({width})
    self.set_height({height})

    # Setup physics
    pw = self.get_phys_world()
    pw.set_gravity({gravity})
    pw.set_scale({scale})

    # Setup backgrounds
    {backgrounds}

    # Setup views
    {views}

    {initCode}
def start(self):
    {startCode}
def end(self):
    {endCode}
